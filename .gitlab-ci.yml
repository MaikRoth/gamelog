include:
  - template: Security/Container-Scanning.gitlab-ci.yml
  - project: "the-microservice-dungeon/devops-team/common-ci-cd"
    ref: "main"
    file: "helm/package-publish.yaml"

stages:
  - test
  - containerize
  - deploy
  - helm

variables:
  # This forces GitLab to only clone the latest commit of the current branch when running the pipeline.
  # This improves speed and reliability because it limits the amount of stuff that needs to be cloned on every run.
  GIT_DEPTH: 1
  # This will suppress any download for dependencies and plugins or upload messages which would clutter the console log.
  # `showDateTime` will show the passed time in milliseconds. You need to specify `--batch-mode` to make this work.
  MAVEN_OPTS: "-Dhttps.protocols=TLSv1.2 -Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=WARN -Dorg.slf4j.simpleLogger.showDateTime=true -Djava.awt.headless=true"
  # As of Maven 3.3.0 instead of this you may define these options in `.mvn/maven.config` so the same config is used
  # when running from the command line.
  # `installAtEnd` and `deployAtEnd` are only effective with recent version of the corresponding plugins.
  MAVEN_CLI_OPTS: "--batch-mode --errors --fail-at-end --show-version -DinstallAtEnd=true -DdeployAtEnd=true"


  # Set Image reference path globally to avoid duplicates
  IMAGE_REF: ${CI_REGISTRY_IMAGE}
  # The directory of the Helm Chart 
  PATH_TO_CHART: "helm-chart"
  # The name of our Chart
  CHART_NAME: "gamelog"

# This template uses jdk17 for verifying and deploying images
image: maven:3.8.5-eclipse-temurin-17

# Cache downloaded dependencies and plugins between builds.
# To keep cache across branches add 'key: "$CI_JOB_NAME"'
cache:
  paths:
    - .m2/repository

# Test stage

# run unit tests (also on merge requests)
test:
  image: maven:3.8.5-eclipse-temurin-17
  stage: test
  services:
    - name: mariadb
      alias: mariadb
    - name: docker.redpanda.com/vectorized/redpanda:latest
      alias: redpanda
      command: ["redpanda", "start", "--overprovisioned", "--node-id", "0", "--check=false"]

  variables:
    KAFKA_BOOTSTRAP_ADDRESS: "redpanda:9092"
    MYSQL_ROOT_PASSWORD: v3ryS1cur3P4ssw0rd!
    MYSQL_HOST: mariadb
    MYSQL_DATABASE: gamelog
    DB_HOST: mariadb
    DB_PASSWORD: v3ryS1cur3P4ssw0rd!
    DB_NAME: gamelog
    DB_USER: root
  # Cache downloaded dependencies and plugins between builds.
  # To keep cache across branches add 'key: "$CI_JOB_NAME"'
  cache:
    paths:
      - .m2/repository
  script:
    - 'mvn $MAVEN_CLI_OPTS test -Psurefire'
    - cat target/site/jacoco/index.html | grep -o '<tfoot>.*</tfoot>'
  artifacts:
    name: 'Test reports'
    when: always
    paths:
      - target/site/jacoco/
    #   - target/surefire-reports/TEST-*.xml
  coverage: '/Total.*?([0-9]{1,3})%/'


# Analyze stage

# This job runs qodana as a static analysis tool and prepare the result to be deployed to gitlab pages, but only on changes to the main branch.
# If there are more problems than defined as the failThreshold in qodana.yml this job will fail and stop the pipe
qodana:
  only:
    variables:
      - $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  needs: [ ]
  stage: test
  image:
    name: jetbrains/qodana-jvm-community:2022.2
    entrypoint: [ "" ]
  script:
    - /opt/idea/bin/entrypoint --results-dir=$CI_PROJECT_DIR/qodana --save-report --report-dir=$CI_PROJECT_DIR/public
  artifacts:
    when: always
    paths:
      - public/
    expose_as: 'Qodana report'

# It has to be named `pages` so gitlab can deploy the results.
# This will run in parallel to other jobs in the pipe and start instantly.
pages:
  needs: [ "qodana" ]
  only:
    variables:
      - $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  when: always
  stage: test
  script: ls public/
  artifacts:
    paths:
      - public


# containerize stage

# Use Jib to build an image
containerize:
  stage: containerize
  variables:
    IMAGE_REF: ${CI_REGISTRY_IMAGE}:${CI_COMMIT_TAG}
  script:
    - >
      mvn compile com.google.cloud.tools:jib-maven-plugin:3.1.4:build
      -Djib.to.image=${IMAGE_REF}
      -Djib.from.image=eclipse-temurin:17-alpine
      -Djib.to.auth.username=${CI_REGISTRY_USER}
      -Djib.to.auth.password=${CI_REGISTRY_PASSWORD}
  rules:
    # Only build on new versions
    - if: $CI_COMMIT_TAG
      when: on_success


deploy:production:
  image: curlimages/curl:latest
  stage: deploy
  needs:
    - job: containerize
      artifacts: true
  variables:
    MANIFEST_PROJECT_ID: 35260662 # k8s-manifest id
    ENVIRONMENT: production
    IMAGE_REF: ${CI_REGISTRY_IMAGE}:${CI_COMMIT_TAG}
  script:
  - >
    curl --fail --request POST
    --form token=${CI_JOB_TOKEN}
    --form ref=main
    --form "variables[IMAGE_REF]=${IMAGE_REF}"
    --form "variables[TARGET_ENVIRONMENT]=${TARGET_ENVIRONMENT}"
    --form "variables[IMAGE_REF]=${IMAGE_REF}"
    --form "variables[SERVICE_NAME]=${CI_PROJECT_NAME}"
    --form "variables[ORIGIN_COMMIT_TAG]=${CI_COMMIT_TAG}"
    "${CI_API_V4_URL}/projects/${MANIFEST_PROJECT_ID}/trigger/pipeline"
  # pass variables with --form "variables[downstreamName]=${variableNameInCI}"
  environment:
    name: $ENVIRONMENT
  rules:
    - if: '$CI_COMMIT_TAG'
      when: on_success


helm-package-publish:
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
      changes:
        - ${PATH_TO_CHART}/**/*
    - if: '$CI_PIPELINE_SOURCE == "web"'
      when: always
