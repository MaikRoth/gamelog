package io.archilab.themicroservicedungeon.gamelog.adapters.api

import io.archilab.themicroservicedungeon.gamelog.domain.dtos.AchievementsDto
import io.archilab.themicroservicedungeon.gamelog.domain.repositories.AchievementDtosRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.util.*

@RestController
@CrossOrigin
@RequestMapping("achievements")
class AchievementsApi {

    @Autowired
    private lateinit var achievementsRepository: AchievementDtosRepository

    @GetMapping(produces = ["application/json"])
    fun getAchievements(): ResponseEntity<AchievementsDto> {
        val achievements = achievementsRepository.getAchievements()
        return ResponseEntity(achievements, HttpStatus.OK)
    }

}