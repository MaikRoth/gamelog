package io.archilab.themicroservicedungeon.gamelog.adapters.api

import io.archilab.themicroservicedungeon.gamelog.domain.dtos.MapDto
import io.archilab.themicroservicedungeon.gamelog.domain.repositories.MapRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@CrossOrigin
@RequestMapping("map")
class MapApi {

    @Autowired
    private lateinit var mapRepository: MapRepository

    @GetMapping(produces = ["application/json"])
    fun getMap(): ResponseEntity<MapDto> {
        val map = mapRepository.getMap()
        return ResponseEntity(map, HttpStatus.OK)
    }

}