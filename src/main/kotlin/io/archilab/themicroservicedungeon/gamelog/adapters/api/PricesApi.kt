package io.archilab.themicroservicedungeon.gamelog.adapters.api

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import io.archilab.themicroservicedungeon.gamelog.domain.dtos.PricesListDto
import io.archilab.themicroservicedungeon.gamelog.domain.entities.PricesList
import io.archilab.themicroservicedungeon.gamelog.domain.repositories.PriceRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.util.*

@RestController
@CrossOrigin
@RequestMapping("prices")
class PricesApi {

    @Autowired
    private lateinit var priceRepository: PriceRepository

    @GetMapping(produces = ["application/json"])
    @JsonIgnoreProperties("id")
    fun getAchievements(): ResponseEntity<PricesListDto> {
        val pricesList = priceRepository.getNewestPrices()?: PricesList()
        return ResponseEntity(PricesListDto(pricesList), HttpStatus.OK)
    }

}