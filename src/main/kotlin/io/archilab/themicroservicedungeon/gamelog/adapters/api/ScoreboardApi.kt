package io.archilab.themicroservicedungeon.gamelog.adapters.api

import io.archilab.themicroservicedungeon.gamelog.domain.dtos.ScoreboardDto
import io.archilab.themicroservicedungeon.gamelog.domain.repositories.ScoreboardRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@CrossOrigin
@RequestMapping("scoreboard")
class ScoreboardApi {

    @Autowired
    private lateinit var scoreboardRepository: ScoreboardRepository

    @GetMapping(produces = ["application/json"])
    fun getScoreboard(): ResponseEntity<ScoreboardDto> {
        val scoreboard = scoreboardRepository.getScoreboard()
        return ResponseEntity(scoreboard, HttpStatus.OK)
    }

}