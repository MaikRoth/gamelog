package io.archilab.themicroservicedungeon.gamelog.adapters.database.interfaces

import io.archilab.themicroservicedungeon.gamelog.domain.entities.Player
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface PlayerCrudRepository : CrudRepository<Player, UUID> {
}