package io.archilab.themicroservicedungeon.gamelog.adapters.database.interfaces

import io.archilab.themicroservicedungeon.gamelog.domain.entities.PricesList
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface PricesListCrudRepository : CrudRepository<PricesList, Long> {

    fun findFirstByOrderByTimestampDesc(): PricesList?

}