package io.archilab.themicroservicedungeon.gamelog.adapters.database.interfaces

import io.archilab.themicroservicedungeon.gamelog.domain.entities.Robot
import org.springframework.data.repository.CrudRepository
import java.util.UUID

interface RobotCrudRepository : CrudRepository<Robot, UUID> {

    fun findRobotsByCurrentPlanetId(planetId: UUID) : List<Robot>

}