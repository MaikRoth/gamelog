package io.archilab.themicroservicedungeon.gamelog.adapters.database.mocks

import io.archilab.themicroservicedungeon.gamelog.domain.achievements.AchievementsManager
import io.archilab.themicroservicedungeon.gamelog.domain.dtos.*
import io.archilab.themicroservicedungeon.gamelog.domain.enums.AchievementType
import io.archilab.themicroservicedungeon.gamelog.domain.enums.GameStatus
import io.archilab.themicroservicedungeon.gamelog.domain.repositories.ScoreboardWithAchievementsRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository
import java.util.*
import kotlin.random.Random

@Repository
@Profile("api-test")
class ScoreboardWithAchievementsRepositoryMock : ScoreboardWithAchievementsRepository {

    @Autowired
    private lateinit var achievementsManager: AchievementsManager

    private val playerNames = listOf(
        "foo",
        "bar",
        "baz",
        "lorem",
        "ipsum"
    )

    override fun getScoreboardWithAchievements(): ScoreboardWithAchievementsDto {
        val gameId = UUID.randomUUID()
        val scoreboardEntriesWithAchievements = generateRandomScoreboardEntries(gameId)
        return ScoreboardWithAchievementsDto(
            gameId,
            GameStatus.STARTED,
            Random.nextInt(0, 100),
            scoreboardEntriesWithAchievements
        )
    }

    private fun generateRandomScoreboardEntries(gameId: UUID): List<ScoreboardEntryWithAchievementsDto> {
        return playerNames.map {
            val player = PlayerDto(
                UUID.randomUUID(),
                it
            )
            ScoreboardEntryWithAchievementsDto(
                player,
                generateRandomScore(),
                generateRandomScore(),
                generateRandomScore(),
                generateRandomScore(),
                generateRandomScore(),
                generateRandomAchievements(player, gameId)
            )
        }
    }

    fun generateRandomScore(): Int {
        return Random.nextInt(1, 100)
    }

    fun generateRandomAchievements(player: PlayerDto, gameId: UUID): List<PlayerAchievementDto> {
        return (0..Random.nextInt(1, 5)).mapNotNull {
            generateRandomAchievement()?.let { achievementDto ->
                PlayerAchievementDto(
                    player,
                    gameId,
                    achievementDto
                )
            }
        }.distinct()
    }

    private fun generateRandomAchievement(): AchievementDto? {
        val achievementType = listOf(
            AchievementType.FIGHTING_BRONZE,
            AchievementType.MINING_BRONZE,
            AchievementType.TRADING_BRONZE,
            AchievementType.TRAVELING_BRONZE
        ).random()
        return achievementsManager.getAchievement(achievementType)?.let {
            AchievementDto(it.name, it.imageUrl, achievementsManager.getAchievementCategory(achievementType))
        }
    }

}