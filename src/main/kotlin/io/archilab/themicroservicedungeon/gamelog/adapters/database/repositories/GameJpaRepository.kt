package io.archilab.themicroservicedungeon.gamelog.adapters.database.repositories

import io.archilab.themicroservicedungeon.gamelog.adapters.database.interfaces.GameCrudRepository
import io.archilab.themicroservicedungeon.gamelog.domain.entities.Game
import io.archilab.themicroservicedungeon.gamelog.domain.repositories.GameRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Repository
import java.time.Instant
import java.util.*

@Repository
class GameJpaRepository : GameRepository {

    @Autowired
    private lateinit var gameCrudRepository: GameCrudRepository

    override fun getGameById(gameId: UUID): Game? {
        return gameCrudRepository.findByIdOrNull(gameId)
    }

    override fun saveGame (game: Game) {
        gameCrudRepository.save(game)
    }

    override fun getGameRunningAtTimestamp(timestamp: Instant): Game? {
        return gameCrudRepository.findByTimestampBetweenStartedAtAndEndedAt(timestamp).firstOrNull()
    }

    override fun getNewestUnfinishedGame(): Game? {
        return gameCrudRepository.findGamesByEndedAtIsNullOrderByStartedAtDesc().firstOrNull()
    }

    override fun getNewestGame(): Game? {
        return gameCrudRepository.findGamesByEndedAtIsNullOrderByStartedAtDesc().firstOrNull()
            ?: gameCrudRepository.findGamesByEndedAtIsNotNullOrderByStartedAtDesc().firstOrNull()
    }

    override fun getUnfinishedGames(): List<Game> {
        return gameCrudRepository.findGamesByEndedAtIsNull()
    }

    override fun getLastFinishedGame(): Game? {
        return gameCrudRepository.findGamesByEndedAtIsNotNullOrderByStartedAtDesc().firstOrNull()
    }

}