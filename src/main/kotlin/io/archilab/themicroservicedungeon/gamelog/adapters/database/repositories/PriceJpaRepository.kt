package io.archilab.themicroservicedungeon.gamelog.adapters.database.repositories

import io.archilab.themicroservicedungeon.gamelog.adapters.database.interfaces.PricesListCrudRepository
import io.archilab.themicroservicedungeon.gamelog.domain.entities.PricesList
import io.archilab.themicroservicedungeon.gamelog.domain.repositories.PriceRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Repository
@Profile("!api-test")
class PriceJpaRepository : PriceRepository {

    @Autowired
    private lateinit var pricesListCrudRepository: PricesListCrudRepository

    override fun getNewestPrices(): PricesList? {
        return pricesListCrudRepository.findFirstByOrderByTimestampDesc()
    }

    override fun savePricesList(pricesList: PricesList) {
        pricesListCrudRepository.save(pricesList)
    }

}