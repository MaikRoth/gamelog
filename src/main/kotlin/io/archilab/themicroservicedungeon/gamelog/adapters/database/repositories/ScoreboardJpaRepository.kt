package io.archilab.themicroservicedungeon.gamelog.adapters.database.repositories

import io.archilab.themicroservicedungeon.gamelog.domain.dtos.PlayerDto
import io.archilab.themicroservicedungeon.gamelog.domain.dtos.ScoreboardDto
import io.archilab.themicroservicedungeon.gamelog.domain.dtos.ScoreboardEntryDto
import io.archilab.themicroservicedungeon.gamelog.domain.entities.Scoreboard
import io.archilab.themicroservicedungeon.gamelog.domain.enums.GameStatus
import io.archilab.themicroservicedungeon.gamelog.domain.repositories.GameRepository
import io.archilab.themicroservicedungeon.gamelog.domain.repositories.ScoreboardRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository
import java.util.*

@Repository
@Profile("!api-test")
class ScoreboardJpaRepository : ScoreboardRepository {

    @Autowired
    private lateinit var gameRepository: GameRepository

    override fun getScoreboard(): ScoreboardDto? {
        val game = gameRepository.getNewestUnfinishedGame()
        return mapScoreboardToScoreboardDto(game?.id, game?.scoreboard, game?.gameStatus)
    }

    private fun mapScoreboardToScoreboardDto(gameId: UUID?, scoreboard: Scoreboard?, gameStatus: GameStatus?): ScoreboardDto {
        return scoreboard?.run {
            val playerDtos = playerScores.map { playerScore ->
                ScoreboardEntryDto(
                    playerScore.player.run { PlayerDto(id, name) },
                    playerScore.totalScore,
                    playerScore.fightingScore,
                    playerScore.miningScore,
                    playerScore.tradingScore,
                    playerScore.travelingScore
                )
            }
            ScoreboardDto(gameId, gameStatus, roundNumber,playerDtos)
        }?: ScoreboardDto(gameId, gameStatus, null, listOf())
    }

}