package io.archilab.themicroservicedungeon.gamelog.adapters.database.repositories

import io.archilab.themicroservicedungeon.gamelog.domain.dtos.ScoreboardEntryWithAchievementsDto
import io.archilab.themicroservicedungeon.gamelog.domain.dtos.ScoreboardWithAchievementsDto
import io.archilab.themicroservicedungeon.gamelog.domain.repositories.AchievementDtosRepository
import io.archilab.themicroservicedungeon.gamelog.domain.repositories.ScoreboardWithAchievementsRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Repository
@Profile("!api-test")
class ScoreboardWithAchievementsJpaRepository : ScoreboardWithAchievementsRepository {

    @Autowired
    private lateinit var achievementDtosRepository: AchievementDtosRepository

    @Autowired
    private lateinit var scoreboardJpaRepository: ScoreboardJpaRepository

    override fun getScoreboardWithAchievements(): ScoreboardWithAchievementsDto {
        val achievements = achievementDtosRepository.getAchievements()
        val scoreboard = scoreboardJpaRepository.getScoreboard()
        return scoreboard?.let { scoreboardDto ->
            ScoreboardWithAchievementsDto(
                scoreboardDto.gameId,
                scoreboardDto.gameStatus,
                scoreboardDto.roundNumber,
                scoreboardDto.scoreboardEntries.map { scoreboardEntryDto ->
                    ScoreboardEntryWithAchievementsDto(
                        scoreboardEntryDto.player,
                        scoreboardEntryDto.totalScore,
                        scoreboardEntryDto.fightingScore,
                        scoreboardEntryDto.miningScore,
                        scoreboardEntryDto.tradingScore,
                        scoreboardEntryDto.travelingScore,
                        achievements.playerAchievements.filter {
                            playerAchievementDto -> playerAchievementDto.player.id == scoreboardEntryDto.player.id
                        }
                    )
                }
            )
        }?: ScoreboardWithAchievementsDto(null, null, null, listOf())
    }

}