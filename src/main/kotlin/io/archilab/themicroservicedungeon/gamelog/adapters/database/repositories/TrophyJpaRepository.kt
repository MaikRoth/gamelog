package io.archilab.themicroservicedungeon.gamelog.adapters.database.repositories

import io.archilab.themicroservicedungeon.gamelog.adapters.database.interfaces.TrophyCrudRepository
import io.archilab.themicroservicedungeon.gamelog.domain.entities.Trophy
import io.archilab.themicroservicedungeon.gamelog.domain.enums.TrophyType
import io.archilab.themicroservicedungeon.gamelog.domain.repositories.TrophyRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Repository

@Repository
class TrophyJpaRepository : TrophyRepository {

    @Autowired
    private lateinit var trophyCrudRepository: TrophyCrudRepository

    override fun store(trophy: Trophy) {
        trophyCrudRepository.save(trophy)
    }

    override fun retrieve(trophyType: TrophyType): Trophy? {
        return trophyCrudRepository.findByIdOrNull(trophyType.id)
    }

}