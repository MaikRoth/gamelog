package io.archilab.themicroservicedungeon.gamelog.adapters.database.repositories

import io.archilab.themicroservicedungeon.gamelog.adapters.database.interfaces.WorldCrudRepository
import io.archilab.themicroservicedungeon.gamelog.domain.entities.World
import io.archilab.themicroservicedungeon.gamelog.domain.enums.WorldStatus
import io.archilab.themicroservicedungeon.gamelog.domain.repositories.WorldRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Repository
import java.util.*

@Repository
class WorldJpaRepository : WorldRepository {

    @Autowired
    private lateinit var worldCrudRepository: WorldCrudRepository

    override fun getWorldById(worldId: UUID): World? {
        return worldCrudRepository.findByIdOrNull(worldId)
    }

    override fun getActiveWorld(): World? {
        return worldCrudRepository.findWorldsByStatus(WorldStatus.ACTIVE).firstOrNull()
    }

    override fun save(world: World) {
        worldCrudRepository.save(world)
    }

}