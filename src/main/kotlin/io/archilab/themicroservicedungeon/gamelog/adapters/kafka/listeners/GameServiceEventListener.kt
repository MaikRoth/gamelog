package io.archilab.themicroservicedungeon.gamelog.adapters.kafka.listeners

import io.archilab.themicroservicedungeon.gamelog.adapters.kafka.mappers.GameStatusEventMapper
import io.archilab.themicroservicedungeon.gamelog.adapters.kafka.mappers.PlayerStatusEventMapper
import io.archilab.themicroservicedungeon.gamelog.adapters.kafka.mappers.RoundStatusEventMapper
import io.archilab.themicroservicedungeon.gamelog.domain.broker.InternalEventPublisher
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.messaging.handler.annotation.Header
import org.springframework.stereotype.Component

@Component
@Profile("!api-test & !no-kafka")
class GameServiceEventListener {

    @Autowired
    private lateinit var roundStatusEventMapper: RoundStatusEventMapper

    @Autowired
    private lateinit var gameStatusEventMapper: GameStatusEventMapper

    @Autowired
    private lateinit var playerStatusEventMapper: PlayerStatusEventMapper

    @Autowired
    private lateinit var internalEventPublisher: InternalEventPublisher

    @KafkaListener(topics = ["status"])
    fun gameStatusEvent(@Header("type") type: String, @Header("timestamp") timestamp: String, payload: ConsumerRecord<String, ByteArray>) {
        val event = gameStatusEventMapper.mapConsumerRecordToEvent(type, timestamp, payload)
        internalEventPublisher.publishEvent(event)
    }

    @KafkaListener(topics = ["roundStatus"])
    fun roundStatusEvent(@Header("type") type: String, @Header("timestamp") timestamp: String, payload: ConsumerRecord<String, ByteArray>) {
        val event = roundStatusEventMapper.mapConsumerRecordToEvent(type, timestamp, payload)
        internalEventPublisher.publishEvent(event)
    }

    @KafkaListener(topics = ["playerStatus"])
    fun playerStatusEvent(@Header("type") type: String, @Header("timestamp") timestamp: String, payload: ConsumerRecord<String, ByteArray>) {
        val event = playerStatusEventMapper.mapConsumerRecordToEvent(type, timestamp, payload)
        internalEventPublisher.publishEvent(event)
    }

}