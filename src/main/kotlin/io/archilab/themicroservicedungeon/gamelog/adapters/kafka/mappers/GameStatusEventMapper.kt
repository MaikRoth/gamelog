package io.archilab.themicroservicedungeon.gamelog.adapters.kafka.mappers

import io.archilab.themicroservicedungeon.gamelog.domain.events.InternalEvent
import io.archilab.themicroservicedungeon.gamelog.domain.events.game.GameStatusEvent
import io.archilab.themicroservicedungeon.gamelog.domain.exceptions.MapperUnknownEventTypeException
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.springframework.stereotype.Component
import java.time.Instant

@Component
class GameStatusEventMapper : KafkaToInternalEventMapper {

    private val objectMapper = InjectingObjectMapper()

    override fun mapConsumerRecordToEvent(type: String, timestamp: String, payload: ConsumerRecord<String, ByteArray>): InternalEvent {
        if (type != "GameStatus") throw MapperUnknownEventTypeException()
        return objectMapper.mapInjectingValues(mapOf(Pair("timestamp", Instant.parse(timestamp))), payload.value(), GameStatusEvent::class.java)
    }

}
