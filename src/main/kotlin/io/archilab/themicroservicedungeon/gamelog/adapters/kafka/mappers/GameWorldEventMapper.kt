package io.archilab.themicroservicedungeon.gamelog.adapters.kafka.mappers

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import io.archilab.themicroservicedungeon.gamelog.domain.exceptions.MapperUnknownEventTypeException
import io.archilab.themicroservicedungeon.gamelog.domain.events.InternalEvent
import io.archilab.themicroservicedungeon.gamelog.domain.events.map.GameWorldCreatedEvent
import io.archilab.themicroservicedungeon.gamelog.domain.events.map.GameWorldDeletedEvent
import io.archilab.themicroservicedungeon.gamelog.domain.events.map.GameWorldStatusChangedEvent
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.springframework.stereotype.Component

@Component
class GameWorldEventMapper : KafkaToInternalEventMapper {

    private val objectMapper = ObjectMapper().registerKotlinModule()

    override fun mapConsumerRecordToEvent(type: String, timestamp: String, payload: ConsumerRecord<String, ByteArray>): InternalEvent {
        val eventClass = when (type) {
            "GameworldCreated" -> GameWorldCreatedEvent::class.java
            "GameworldStatusChanged" -> GameWorldStatusChangedEvent::class.java
            "gameworld-deleted" -> GameWorldDeletedEvent::class.java
            else -> throw MapperUnknownEventTypeException()
        }
        return objectMapper.readValue(payload.value(), eventClass)
    }

}