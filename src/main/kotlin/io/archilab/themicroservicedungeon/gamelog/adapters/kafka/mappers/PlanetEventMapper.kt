package io.archilab.themicroservicedungeon.gamelog.adapters.kafka.mappers

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import io.archilab.themicroservicedungeon.gamelog.domain.exceptions.MapperUnknownEventTypeException
import io.archilab.themicroservicedungeon.gamelog.domain.events.InternalEvent
import io.archilab.themicroservicedungeon.gamelog.domain.events.map.PlanetDiscoveredEvent
import io.archilab.themicroservicedungeon.gamelog.domain.events.map.ResourceMinedEvent
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.springframework.stereotype.Component

@Component
class PlanetEventMapper : KafkaToInternalEventMapper {

    private val objectMapper = ObjectMapper().registerKotlinModule()

    override fun mapConsumerRecordToEvent(type: String, timestamp: String, payload: ConsumerRecord<String, ByteArray>): InternalEvent {
        val className = when(type) {
            "ResourceMined" -> ResourceMinedEvent::class.java
            "PlanetDiscovered" -> PlanetDiscoveredEvent::class.java
            else -> throw MapperUnknownEventTypeException()
        }
        return objectMapper.readValue(payload.value(), className)
    }

}