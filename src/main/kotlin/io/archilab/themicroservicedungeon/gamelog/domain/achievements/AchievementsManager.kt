package io.archilab.themicroservicedungeon.gamelog.domain.achievements

import io.archilab.themicroservicedungeon.gamelog.domain.entities.Achievement
import io.archilab.themicroservicedungeon.gamelog.domain.enums.AchievementCategory
import io.archilab.themicroservicedungeon.gamelog.domain.enums.AchievementType
import io.archilab.themicroservicedungeon.gamelog.domain.repositories.AchievementRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class AchievementsManager {

    @Autowired
    private lateinit var achievementRepository: AchievementRepository

    private val achievements = listOf(
        Achievement(
            AchievementType.FIGHTING_BRONZE.id, "First Blood",
            "https://gitlab.com/the-microservice-dungeon/core-services/gamelog/-/raw/main/assets/achievements/Fighting%20Bronze%20-%20First%20Blood.png"
        ),
        Achievement(
            AchievementType.FIGHTING_SILVER.id, "Blood Thirst",
            "https://gitlab.com/the-microservice-dungeon/core-services/gamelog/-/raw/main/assets/achievements/Fighting%20Silver%20-%20Blood%20Thirst.png"
        ),        Achievement(
            AchievementType.FIGHTING_GOLD.id, "Psychopath",
            "https://gitlab.com/the-microservice-dungeon/core-services/gamelog/-/raw/main/assets/achievements/Fighting%20Gold%20-%20Psychopath.png"
        ),
        Achievement(
            AchievementType.MINING_BRONZE.id, "Miner",
            "https://gitlab.com/the-microservice-dungeon/core-services/gamelog/-/raw/main/assets/achievements/Mining%20Bronze%20-%20Miner.png"
        ),
        Achievement(
            AchievementType.MINING_SILVER.id, "Gold Digger",
            "https://gitlab.com/the-microservice-dungeon/core-services/gamelog/-/raw/main/assets/achievements/Mining%20Silver%20-%20Gold%20Digger.png"
        ),        Achievement(
            AchievementType.MINING_GOLD.id, "Mining Industrialist",
            "https://gitlab.com/the-microservice-dungeon/core-services/gamelog/-/raw/main/assets/achievements/Mining%20Gold%20-%20Mining%20Industrialist.png"
        ),
        Achievement(
            AchievementType.TRADING_BRONZE.id, "Businessman",
            "https://gitlab.com/the-microservice-dungeon/core-services/gamelog/-/raw/main/assets/achievements/Trading%20Bronze%20-%20Businessman.png"
        ),
        Achievement(
            AchievementType.TRADING_SILVER.id, "Manager",
            "https://gitlab.com/the-microservice-dungeon/core-services/gamelog/-/raw/main/assets/achievements/Trading%20Silver%20-%20Manager.png"
        ),        Achievement(
            AchievementType.TRADING_GOLD.id, "Grand Nagus",
            "https://gitlab.com/the-microservice-dungeon/core-services/gamelog/-/raw/main/assets/achievements/Trading%20Gold%20-%20Grand%20Nagus.png"
        ),
        Achievement(
            AchievementType.TRAVELING_BRONZE.id, "Sputnik",
            "https://gitlab.com/the-microservice-dungeon/core-services/gamelog/-/raw/main/assets/achievements/Traveling%20Bronze%20-%20Sputnik.png"
        ),
        Achievement(
            AchievementType.TRAVELING_SILVER.id, "Apollo 11",
            "https://gitlab.com/the-microservice-dungeon/core-services/gamelog/-/raw/main/assets/achievements/Traveling%20Silver%20-%20Apollo%2011.png"
        ),        Achievement(
            AchievementType.TRAVELING_GOLD.id, "Enterprise",
            "https://gitlab.com/the-microservice-dungeon/core-services/gamelog/-/raw/main/assets/achievements/Traveling%20Gold%20-%20Enterprise.png"
        )
    )

    fun getAchievement(type: AchievementType) : Achievement? {
        return achievementRepository.retrieve(type) ?: achievements.find { achievement -> achievement.id == type.id }?.also {
            achievementRepository.store(it)
        }
    }

    fun getAchievementCategory(achievementType: AchievementType): AchievementCategory {
        return when (achievementType) {
            AchievementType.FIGHTING_BRONZE, AchievementType.FIGHTING_SILVER, AchievementType.FIGHTING_GOLD -> AchievementCategory.FIGHTING
            AchievementType.MINING_BRONZE, AchievementType.MINING_SILVER, AchievementType.MINING_GOLD -> AchievementCategory.MINING
            AchievementType.TRADING_BRONZE, AchievementType.TRADING_SILVER, AchievementType.TRADING_GOLD -> AchievementCategory.TRADING
            AchievementType.TRAVELING_BRONZE, AchievementType.TRAVELING_SILVER, AchievementType.TRAVELING_GOLD -> AchievementCategory.TRAVELING
        }
    }

    fun getAchievementCategory(id: Int) : AchievementCategory? {
        val achievementType = getAchievementType(id)
        return achievementType?.let { getAchievementCategory(achievementType) }
    }

    private fun getAchievementType(id: Int): AchievementType? {
        return AchievementType.values().find { achievementType -> achievementType.id == id }
    }

}