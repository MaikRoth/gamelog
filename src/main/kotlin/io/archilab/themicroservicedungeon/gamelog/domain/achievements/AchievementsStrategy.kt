package io.archilab.themicroservicedungeon.gamelog.domain.achievements

import io.archilab.themicroservicedungeon.gamelog.domain.entities.Player
import io.archilab.themicroservicedungeon.gamelog.domain.enums.AchievementType
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import java.math.BigDecimal
import java.util.*

@Component
class AchievementsStrategy {

    @Autowired
    private lateinit var achievementsManager: AchievementsManager

    fun awardAchievementsToPlayer(gameId: UUID, player: Player) {
        achievementsRequirements.forEach { (type, requirement) ->
            if (!hasAchievement(player, type)) {
                if (requirement(player)) {
                    achievementsManager.getAchievement(type)?.let { achievement ->
                        player.awardAchievement(gameId, achievement)
                    }
                }
            }
        }
    }

    private fun hasAchievement(player: Player, type: AchievementType): Boolean {
        return player.achievements.any { playerAchievement ->
            playerAchievement.achievement.id == type.id
        }
    }

    private val fightingBronzeRequirements = { player: Player -> player.killsCount >= 1 }

    private val fightingSilverRequirements = { player: Player -> player.killsCount >= 10 }

    private val fightingGoldRequirements = { player: Player -> player.killsCount >= 25 }

    private val miningBronzeRequirements = { player: Player -> player.minedResourcesCount >= 1 }

    private val miningSilverRequirements = { player: Player -> player.minedResourcesCount >= 25 }

    private val miningGoldRequirements = { player: Player -> player.minedResourcesCount >= 150 }

    private val tradingBronzeRequirements = { player: Player -> player.earnedMoneyCount >= BigDecimal(1) }

    private val tradingSilverRequirements = { player: Player -> player.earnedMoneyCount >= BigDecimal(1000) }

    private val tradingGoldRequirements = { player: Player -> player.earnedMoneyCount >= BigDecimal(10000) }

    private val travelingBronzeRequirements = { player: Player -> player.movementsCount >= 1 }

    private val travelingSilverRequirements = { player: Player -> player.movementsCount >= 15 }

    private val travelingGoldRequirements = { player: Player -> player.movementsCount >= 40 }

    private val achievementsRequirements = mapOf<AchievementType, (Player) -> Boolean>(
        AchievementType.FIGHTING_BRONZE to fightingBronzeRequirements,
        AchievementType.FIGHTING_SILVER to fightingSilverRequirements,
        AchievementType.FIGHTING_GOLD to fightingGoldRequirements,
        AchievementType.MINING_BRONZE to miningBronzeRequirements,
        AchievementType.MINING_SILVER to miningSilverRequirements,
        AchievementType.MINING_GOLD to miningGoldRequirements,
        AchievementType.TRADING_BRONZE to tradingBronzeRequirements,
        AchievementType.TRADING_SILVER to tradingSilverRequirements,
        AchievementType.TRADING_GOLD to tradingGoldRequirements,
        AchievementType.TRAVELING_BRONZE to travelingBronzeRequirements,
        AchievementType.TRAVELING_SILVER to travelingSilverRequirements,
        AchievementType.TRAVELING_GOLD to travelingGoldRequirements
    )

}