package io.archilab.themicroservicedungeon.gamelog.domain.broker

import RobotSpawnedEvent
import io.archilab.themicroservicedungeon.gamelog.domain.eventhandlers.*
import io.archilab.themicroservicedungeon.gamelog.domain.events.InternalEvent
import io.archilab.themicroservicedungeon.gamelog.domain.events.game.GameStatusEvent
import io.archilab.themicroservicedungeon.gamelog.domain.events.game.PlayerStatusEvent
import io.archilab.themicroservicedungeon.gamelog.domain.events.game.RoundStatusEvent
import io.archilab.themicroservicedungeon.gamelog.domain.events.map.*
import io.archilab.themicroservicedungeon.gamelog.domain.events.robot.*
import io.archilab.themicroservicedungeon.gamelog.domain.events.trading.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional

@Component
@Transactional
class InternalEventListener {

    @Autowired
    private lateinit var gameEventHandler: GameEventHandler

    @Autowired
    private lateinit var playerStatusEventHandler: PlayerStatusEventHandler

    @Autowired
    private lateinit var roundStatusEventHandler: RoundStatusEventHandler

    @Autowired
    private lateinit var gameWorldEventHandler: GameWorldEventHandler

    @Autowired
    private lateinit var robotIntegrationEventHandler: RobotIntegrationEventHandler

    @Autowired
    private lateinit var tradingEventHandler: TradingEventHandler

    @EventListener
    fun listenForInternalEvents(event: InternalEvent) {
        when(event) {
            is GameStatusEvent -> gameEventHandler.handleGameStatusEvent(event)
            is PlayerStatusEvent -> playerStatusEventHandler.handlePlayerStatusEvent(event)
            is RoundStatusEvent -> roundStatusEventHandler.handleRoundStatusEvent(event)
            is GameWorldCreatedEvent -> gameWorldEventHandler.handleGameWorldCreatedEvent(event)
            is GameWorldDeletedEvent -> gameWorldEventHandler.handleGameWorldDeletedEvent(event)
            is GameWorldStatusChangedEvent -> gameWorldEventHandler.handleGameWorldStatusChangedEvent(event)
            is ResourceMinedEvent -> gameWorldEventHandler.handleResourceMinedEvent(event)
            is PlanetDiscoveredEvent -> {} // No use for this information. Discard it.
            is BankAccountInitializedEvent -> tradingEventHandler.handleBankAccountInitializedEvent(event)
            is BankAccountClearedEvent -> tradingEventHandler.handleBankAccountClearedEvent(event)
            is BankAccountTransactionBookedEvent -> tradingEventHandler.handleBankAccountTransactionBookedEvent(event)
            is TradablePricesEvent -> tradingEventHandler.handleTradablePricesEvent(event)
            is TradableBoughtEvent -> tradingEventHandler.handleTradableBoughtEvent(event)
            is TradableSoldEvent -> tradingEventHandler.handleTradableSoldEvent(event)
            is RobotSpawnedEvent -> robotIntegrationEventHandler.handleRobotSpawnedEvent(event)
            is RobotUpgradedEvent -> robotIntegrationEventHandler.handleRobotUpgradedEvent(event)
            is RobotRestoredAttributesEvent -> robotIntegrationEventHandler.handleRobotRestoredAttributesEvent(event)
            is RobotResourceRemovedEvent -> robotIntegrationEventHandler.handleRobotResourceRemovedEvent(event)
            is RobotResourceMinedEvent -> robotIntegrationEventHandler.handleRobotResourceMinedEvent(event)
            is RobotRegeneratedEvent -> robotIntegrationEventHandler.handleRobotRegeneratedEvent(event)
            is RobotMovedEvent -> robotIntegrationEventHandler.handleRobotMovedEvent(event)
            is RobotAttackedEvent -> robotIntegrationEventHandler.handleRobotAttackedEvent(event)
            is RobotsRevealedEvent -> {} // No use for this information. Discard it.
            else -> println(event)
        }
    }

}
