package io.archilab.themicroservicedungeon.gamelog.domain.dtos

import java.util.*

data class AchievementsDto(
    val gameId: UUID?,
    val playerAchievements: List<PlayerAchievementDto>
)