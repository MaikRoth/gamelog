package io.archilab.themicroservicedungeon.gamelog.domain.dtos

import io.archilab.themicroservicedungeon.gamelog.domain.enums.GameStatus
import java.time.Instant
import java.util.UUID

data class MapDto(
    val gameId: UUID?,
    val gameStatus: GameStatus?,
    val roundNumber: Int?,
    val timestamp: Instant,
    val players: List<String>,
    val planets: List<PlanetDto>
)