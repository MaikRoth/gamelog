package io.archilab.themicroservicedungeon.gamelog.domain.dtos

import java.util.*

data class PlanetDto(
    val planetId: UUID,
    val x: Int,
    val y: Int,
    val movementDifficulty: Int,
    val rechargeMultiplicator: Int,
    val resources: List<ResourceDto>,
    val robots: List<RobotDto>
)