package io.archilab.themicroservicedungeon.gamelog.domain.dtos

import java.util.*

data class PlayerAchievementDto(
    val player: PlayerDto,
    val gameId: UUID,
    val achievement: AchievementDto,
)