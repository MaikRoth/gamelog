package io.archilab.themicroservicedungeon.gamelog.domain.dtos

import java.util.*

data class PlayerDto (
    val id: UUID,
    val name: String
)