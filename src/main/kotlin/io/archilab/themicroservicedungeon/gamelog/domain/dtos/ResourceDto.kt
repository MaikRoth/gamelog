package io.archilab.themicroservicedungeon.gamelog.domain.dtos

import io.archilab.themicroservicedungeon.gamelog.domain.enums.ResourceType

data class ResourceDto(
    val resourceType: ResourceType,
    val maxAmount: Int,
    val currentAmount: Int
)