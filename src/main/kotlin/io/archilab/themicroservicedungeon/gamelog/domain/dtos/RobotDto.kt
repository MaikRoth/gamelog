package io.archilab.themicroservicedungeon.gamelog.domain.dtos

import java.util.*

data class RobotDto (
    val robotId: UUID
)