package io.archilab.themicroservicedungeon.gamelog.domain.dtos

import io.archilab.themicroservicedungeon.gamelog.domain.enums.GameStatus
import java.util.*
data class ScoreboardDto (
    val gameId: UUID?,
    val gameStatus: GameStatus?,
    val roundNumber: Int?,
    val scoreboardEntries: List<ScoreboardEntryDto>
)

