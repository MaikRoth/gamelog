package io.archilab.themicroservicedungeon.gamelog.domain.dtos

data class ScoreboardEntryDto (
    val player: PlayerDto,
    val totalScore: Int,
    val fightingScore:Int,
    val miningScore:Int,
    val tradingScore: Int,
    val travelingScore: Int
)