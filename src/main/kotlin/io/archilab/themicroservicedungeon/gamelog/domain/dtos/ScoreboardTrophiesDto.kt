package io.archilab.themicroservicedungeon.gamelog.domain.dtos

import java.util.*

data class ScoreboardTrophiesDto(
    val gameID: UUID?,
    val scoreboardTrophies: List<PlayerScoreboardTrophiesDto>
)