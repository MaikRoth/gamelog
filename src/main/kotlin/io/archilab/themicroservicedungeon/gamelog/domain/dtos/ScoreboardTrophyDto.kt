package io.archilab.themicroservicedungeon.gamelog.domain.dtos

import io.archilab.themicroservicedungeon.gamelog.domain.enums.ScoreboardCategory

data class ScoreboardTrophyDto (
    val name: String,
    val image: String,
    val category: ScoreboardCategory
)