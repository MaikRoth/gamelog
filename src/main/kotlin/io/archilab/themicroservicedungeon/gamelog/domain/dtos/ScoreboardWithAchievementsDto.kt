package io.archilab.themicroservicedungeon.gamelog.domain.dtos

import io.archilab.themicroservicedungeon.gamelog.domain.enums.GameStatus
import java.util.*

data class ScoreboardWithAchievementsDto(
    val gameId: UUID?,
    val gameStatus: GameStatus?,
    val roundNumber: Int?,
    val scoreboardEntriesWithAchievements: List<ScoreboardEntryWithAchievementsDto>,
)