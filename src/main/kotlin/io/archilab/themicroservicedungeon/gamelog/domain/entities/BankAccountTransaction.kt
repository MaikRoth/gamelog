package io.archilab.themicroservicedungeon.gamelog.domain.entities

import io.archilab.themicroservicedungeon.gamelog.domain.enums.BankAccountTransactionType
import java.time.Instant
import javax.persistence.Column
import javax.persistence.Embeddable

@Embeddable
class BankAccountTransaction() {

    @Column
    lateinit var type: BankAccountTransactionType

    @Column
    var amount: Long = 0

    @Column
    var balance: Long = 0

    @Column
    lateinit var timestamp: Instant

    constructor(type: BankAccountTransactionType, amount: Long, balance: Long, timestamp: Instant): this() {
        this.type = type
        this.amount = amount
        this.balance = balance
        this.timestamp = timestamp
    }

}