package io.archilab.themicroservicedungeon.gamelog.domain.entities

import io.archilab.themicroservicedungeon.gamelog.domain.enums.BankAccountTransactionType
import io.archilab.themicroservicedungeon.gamelog.domain.enums.TradableType
import io.archilab.themicroservicedungeon.gamelog.domain.enums.TradingTransactionType
import org.hibernate.annotations.Type
import java.math.BigDecimal
import java.time.Instant
import java.util.*
import javax.persistence.*

@Entity
class Player() {

    @Id
    @Column
    @Type(type = "uuid-char")
    lateinit var id: UUID

    @Column
    lateinit var name: String

    @ManyToMany
    lateinit var games: MutableList<Game>

    @OneToMany(cascade = [CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH])
    lateinit var robots: Set<Robot>

    val bankAccountBalance: Long
        get() {
            return bankAccountTransactions.maxByOrNull { it.timestamp }?.balance?: 0
        }

    @ElementCollection
    var bankAccountTransactions: MutableList<BankAccountTransaction> = mutableListOf()

    @ElementCollection
    var tradingTransactions: MutableList<TradingTransaction> = mutableListOf()

    @ElementCollection
    var trophies: MutableList<PlayerTrophy> = mutableListOf()

    @ElementCollection
    var achievements: MutableList<PlayerAchievement> = mutableListOf()

    val currentGame: Game?
        get() {
            return games.sortedWith(compareByDescending(nullsFirst()) { game -> game.startedAt }).firstOrNull()
        }

    val killsCount: Int
        get() {
            return robots.sumOf {
                it.kills.count()
            }
        }

    val minedResourcesCount: Int
        get() {
            return robots.sumOf {
                it.minedResources.run {
                    coal + iron + gem + gold + platin
                }
            }
        }

    val earnedMoneyCount: BigDecimal
        get() {
            return tradingTransactions.filter {
                    transaction -> transaction.transactionType == TradingTransactionType.SOLD
            }.sumOf {
                it.totalPrice
            }
        }

    val movementsCount: Int
        get() {
            return robots.sumOf {
                it.movements.count()
            }
        }

    constructor(id: UUID, name: String) : this() {
        this.id = id
        this.name = name
        this.robots = setOf()
        this.bankAccountTransactions = mutableListOf()
        this.tradingTransactions = mutableListOf()
        this.games = mutableListOf()
    }

    constructor(id: UUID) : this(id, "AUTO_INITIALIZED")

    fun addRobot(robot: Robot) {
        robot.gameId = this.currentGame?.id
        this.robots = robots.plus(robot)
    }

    fun awardTrophy(gameId: UUID, trophy: Trophy) {
        val newTrophy = PlayerTrophy(gameId, trophy)
        val noDuplicates = trophies.find {it.matches(newTrophy)} == null
        if (noDuplicates) {
            trophies.add(newTrophy)
        }
    }

    fun awardAchievement(gameId: UUID, achievement: Achievement) {
        achievements.add(PlayerAchievement(gameId, achievement))
    }

    fun bookBankAccountTransaction(type: BankAccountTransactionType, amount: Long, balance: Long, timestamp: Instant) {
        this.bankAccountTransactions.add(
            BankAccountTransaction(type, amount, balance, timestamp)
        )
    }

    fun bookBuyingTransaction(robotId: UUID?,
                              tradableType: TradableType,
                              name: String,
                              amount: Int,
                              pricePerUnit: BigDecimal,
                              totalPrice: BigDecimal) {
        tradingTransactions.add(TradingTransaction(robotId, currentGame?.id, tradableType, TradingTransactionType.BOUGHT, name, amount, pricePerUnit, totalPrice))
    }

    fun bookSellingTransaction(robotId: UUID,
                              tradableType: TradableType,
                              name: String,
                              amount: Int,
                              pricePerUnit: BigDecimal,
                              totalPrice: BigDecimal) {
        tradingTransactions.add(TradingTransaction(robotId, currentGame?.id, tradableType, TradingTransactionType.SOLD, name, amount, pricePerUnit, totalPrice))
    }

    fun joinGame(game: Game) {
        games.add(game)
        game.registerPlayer(this)
    }

}

