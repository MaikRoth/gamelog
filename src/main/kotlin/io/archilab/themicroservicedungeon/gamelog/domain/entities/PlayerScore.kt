package io.archilab.themicroservicedungeon.gamelog.domain.entities

import javax.persistence.Column
import javax.persistence.Embeddable
import javax.persistence.ManyToOne

@Embeddable
class PlayerScore () {
    @ManyToOne
    lateinit var player: Player
    @Column
    var totalScore: Int = 0
    @Column
    var fightingScore:Int = 0
    @Column
    var miningScore:Int = 0
    @Column
    var tradingScore: Int = 0
    @Column
    var travelingScore: Int = 0

    constructor(player: Player, totalScore: Int, fightingScore: Int, miningScore: Int, tradingScore: Int, travelingScore: Int): this() {
        this.player = player
        this.totalScore = totalScore
        this.fightingScore = fightingScore
        this.miningScore = miningScore
        this.tradingScore = tradingScore
        this.travelingScore = travelingScore
    }

}