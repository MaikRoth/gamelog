package io.archilab.themicroservicedungeon.gamelog.domain.entities

import java.util.*
import javax.persistence.Column
import javax.persistence.Embeddable
import javax.persistence.ManyToOne

@Embeddable
class PlayerTrophy() {

    @Column
    lateinit var gameId: UUID

    @ManyToOne
    lateinit var trophy: Trophy

    constructor(gameId: UUID, trophy: Trophy) : this() {
        this.gameId = gameId
        this.trophy = trophy
    }

    fun matches(other: PlayerTrophy?): Boolean {
        if (other == null) return false
        return gameId == other.gameId && trophy == other.trophy
    }

}