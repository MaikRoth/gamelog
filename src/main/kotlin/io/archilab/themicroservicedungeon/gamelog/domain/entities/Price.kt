package io.archilab.themicroservicedungeon.gamelog.domain.entities

import io.archilab.themicroservicedungeon.gamelog.domain.enums.TradableType
import java.math.BigDecimal
import javax.persistence.Embeddable

@Embeddable
class Price() {

    lateinit var name: String
    lateinit var price: BigDecimal
    lateinit var type: TradableType

    constructor(name: String, price: BigDecimal, type: TradableType) : this() {
        this.name = name
        this.price = price
        this.type = type
    }
}