package io.archilab.themicroservicedungeon.gamelog.domain.entities

import io.archilab.themicroservicedungeon.gamelog.domain.enums.RoundStatus
import org.hibernate.annotations.Type
import java.time.Instant
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id

@Entity
class Round() {

    @Column
    @Type(type = "uuid-char")
    lateinit var gameId: UUID
        private set

    @Id
    @Column
    @Type(type = "uuid-char")
    lateinit var roundId: UUID
        private set

    @Column
    var roundNumber: Int = 0
        private set

    @Column
    lateinit var roundStatus: RoundStatus
        private set

    @Column
    var startedAt: Instant? = null
        private set

    @Column
    var commandInputEndedAt: Instant? = null
        private set

    @Column
    var endedAt: Instant? = null
        private set

    constructor(gameId: UUID, roundId: UUID, roundNumber: Int, roundStatus: RoundStatus, startedAt: Instant) : this() {
        this.gameId = gameId
        this.roundId = roundId
        this.roundNumber = roundNumber
        this.roundStatus = roundStatus
        this.startedAt = startedAt
    }

    fun commandInputEnded(timestamp: Instant) {
        roundStatus = RoundStatus.COMMAND_INPUT_ENDED
        commandInputEndedAt = timestamp
    }

    fun roundEnded(timestamp: Instant) {
        roundStatus = RoundStatus.ENDED
        endedAt = timestamp
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Round

        if (gameId != other.gameId) return false
        if (roundId != other.roundId) return false
        if (roundNumber != other.roundNumber) return false
        if (roundStatus != other.roundStatus) return false

        return true
    }

    override fun hashCode(): Int {
        var result = gameId.hashCode()
        result = 31 * result + roundId.hashCode()
        result = 31 * result + roundNumber
        result = 31 * result + roundStatus.hashCode()
        return result
    }


}