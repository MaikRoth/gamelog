package io.archilab.themicroservicedungeon.gamelog.domain.entities

import io.archilab.themicroservicedungeon.gamelog.domain.enums.TradableType
import io.archilab.themicroservicedungeon.gamelog.domain.enums.TradingTransactionType
import java.math.BigDecimal
import java.util.*
import javax.persistence.Column
import javax.persistence.Embeddable

@Embeddable
class TradingTransaction() {

    @Column
    var robotId: UUID? = null

    @Column
    var gameId: UUID? = null

    @Column
    lateinit var tradableType: TradableType

    @Column
    lateinit var transactionType: TradingTransactionType

    @Column
    lateinit var name: String

    @Column
    var amount: Int = 0

    @Column
    lateinit var pricePerUnit: BigDecimal

    @Column
    lateinit var totalPrice: BigDecimal

    constructor(
        robotId: UUID?,
        gameId: UUID?,
        tradableType: TradableType,
        transactionType: TradingTransactionType,
        name: String,
        amount: Int,
        pricePerUnit: BigDecimal,
        totalPrice: BigDecimal
    ) : this() {
        this.robotId = robotId
        this.gameId = gameId
        this.tradableType = tradableType
        this.transactionType = transactionType
        this.name = name
        this.amount = amount
        this.pricePerUnit = pricePerUnit
        this.totalPrice = totalPrice
    }

}