package io.archilab.themicroservicedungeon.gamelog.domain.enums

import com.fasterxml.jackson.annotation.JsonValue

enum class RoundStatus (@get:JsonValue val status: String) {
    STARTED("started"),
    COMMAND_INPUT_ENDED("command input ended"),
    ENDED("ended")
}