package io.archilab.themicroservicedungeon.gamelog.domain.enums

enum class ScoreboardCategory {
    FIGHTING,
    GAME,
    MINING,
    TRADING,
    TRAVELING
}