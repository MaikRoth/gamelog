package io.archilab.themicroservicedungeon.gamelog.domain.enums

enum class TrophyCategory {
    FIGHTING,
    GAME,
    MINING,
    TRADING,
    TRAVELING
}