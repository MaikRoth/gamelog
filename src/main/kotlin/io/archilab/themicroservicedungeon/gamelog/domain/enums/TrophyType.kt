package io.archilab.themicroservicedungeon.gamelog.domain.enums

enum class TrophyType(val id: Int) {
    GameFirstPlace(0),
    GameSecondPlace(1),
    GameThirdPlace(2),
    FightingFirstPlace(3),
    FightingSecondPlace(4),
    FightingThirdPlace(5),
    MiningFirstPlace(6),
    MiningSecondPlace(7),
    MiningThirdPlace(8),
    TradingFirstPlace(9),
    TradingSecondPlace(10),
    TradingThirdPlace(11),
    TravelingFirstPlace(12),
    TravelingSecondPlace(13),
    TravelingThirdPlace(14)
}