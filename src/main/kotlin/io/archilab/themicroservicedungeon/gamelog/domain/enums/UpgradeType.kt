package io.archilab.themicroservicedungeon.gamelog.domain.enums

import com.fasterxml.jackson.annotation.JsonValue

enum class UpgradeType (@get:JsonValue val type: String) {
    STORAGE("STORAGE"),
    HEALTH("HEALTH"),
    DAMAGE("DAMAGE"),
    MINING_SPEED("MINING_SPEED"),
    MINING("MINING"),
    MAXIMUM_ENERGY("MAX_ENERGY"),
    ENERGY_REGENERATION("ENERGY_REGEN")
}