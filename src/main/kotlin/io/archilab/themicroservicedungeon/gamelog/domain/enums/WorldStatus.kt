package io.archilab.themicroservicedungeon.gamelog.domain.enums

import com.fasterxml.jackson.annotation.JsonValue

enum class WorldStatus (@get:JsonValue val status: String) {
    ACTIVE("ACTIVE"),
    INACTIVE("INACTIVE")
}