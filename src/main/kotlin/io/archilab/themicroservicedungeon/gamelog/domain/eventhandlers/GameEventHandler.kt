package io.archilab.themicroservicedungeon.gamelog.domain.eventhandlers

import io.archilab.themicroservicedungeon.gamelog.domain.entities.Game
import io.archilab.themicroservicedungeon.gamelog.domain.enums.GameStatus
import io.archilab.themicroservicedungeon.gamelog.domain.events.game.GameStatusEvent
import io.archilab.themicroservicedungeon.gamelog.domain.repositories.GameRepository
import io.archilab.themicroservicedungeon.gamelog.domain.repositories.PlayerRepository
import io.archilab.themicroservicedungeon.gamelog.domain.trophies.TrophiesStrategy
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional
import java.time.Instant
import java.util.*

@Component
class GameEventHandler {

    @Autowired
    private lateinit var gameRepository: GameRepository

    @Autowired
    private lateinit var playerRepository: PlayerRepository

    @Autowired
    private lateinit var trophiesStrategy: TrophiesStrategy

    fun handleGameStatusEvent(event: GameStatusEvent) {
        when (event.status) {
            GameStatus.CREATED -> handleGameCreated(event)
            GameStatus.STARTED -> handleGameStarted(event)
            GameStatus.ENDED -> handleGameEnded(event)
        }
    }

    private fun handleGameCreated(event: GameStatusEvent) {
        getOrCreateGame(event.gameId, event.gameWorldId, event.timestamp)
    }

    @Transactional
    fun getOrCreateGame(gameId: UUID, gameWorldId: UUID?, timestamp: Instant): Game {
        endInactiveGames(gameId, timestamp)
        return gameRepository.getGameById(gameId)?: Game(gameId, gameWorldId, GameStatus.CREATED).apply {
            gameRepository.saveGame(this)
        }
    }

    fun endInactiveGames(runningGameId: UUID, timestamp: Instant) {
        val unfinishedGames = gameRepository.getUnfinishedGames()
        val endTimestamp = timestamp.minusMillis(1)
        unfinishedGames.forEach {game ->
            if (game.id != runningGameId) {
                println("Automatically ending unfinished game ${game.id}, as new game ${runningGameId} is already running!")
                endGameAndAwardTrophies(game, endTimestamp)
            }
        }
    }

    @Transactional
    fun handleGameStarted(event: GameStatusEvent) {
        val game = getOrCreateGame(event.gameId, event.gameWorldId, event.timestamp)
        game.apply {
            startGame(event.timestamp)
            if (gameWorldId == null) {
                gameWorldId = event.gameWorldId
            }
        }.also { gameRepository.saveGame(it) }
    }

    @Transactional
    fun handleGameEnded(event: GameStatusEvent) {
        val game = getOrCreateGame(event.gameId, event.gameWorldId, event.timestamp)
        if (game.gameWorldId == null) {
            game.gameWorldId = event.gameWorldId
        }
        println("Received event to end game ${event.gameId}.")
        endGameAndAwardTrophies(game, event.timestamp)
    }

    private fun endGameAndAwardTrophies(game: Game, timestamp: Instant) {
        game.apply {
            endGame(timestamp)
            trophiesStrategy.awardTrophiesForGame(this)
            players.forEach {
                playerRepository.savePlayer(it)
            }
            gameRepository.saveGame(this)
        }
    }

}