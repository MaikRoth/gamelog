package io.archilab.themicroservicedungeon.gamelog.domain.eventhandlers

import io.archilab.themicroservicedungeon.gamelog.domain.entities.World
import io.archilab.themicroservicedungeon.gamelog.domain.events.map.GameWorldCreatedEvent
import io.archilab.themicroservicedungeon.gamelog.domain.events.map.GameWorldDeletedEvent
import io.archilab.themicroservicedungeon.gamelog.domain.events.map.GameWorldStatusChangedEvent
import io.archilab.themicroservicedungeon.gamelog.domain.events.map.ResourceMinedEvent
import io.archilab.themicroservicedungeon.gamelog.domain.mappers.GameWorldPlanetToPlanetMapper
import io.archilab.themicroservicedungeon.gamelog.domain.repositories.PlanetRepository
import io.archilab.themicroservicedungeon.gamelog.domain.repositories.WorldRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional

@Component
class GameWorldEventHandler {

    @Autowired
    private lateinit var worldRepository: WorldRepository

    @Autowired
    private lateinit var gameWorldPlanetToPlanetMapper: GameWorldPlanetToPlanetMapper

    @Autowired
    private lateinit var planetRepository: PlanetRepository

    @Transactional
    fun handleGameWorldCreatedEvent(event: GameWorldCreatedEvent) {
        val planets = gameWorldPlanetToPlanetMapper.mapList(event.planets)
        val world = World(event.gameWorldId, event.status, planets.toTypedArray().toSet())
        worldRepository.save(world)
    }

    fun handleGameWorldDeletedEvent(event: GameWorldDeletedEvent) {
        // We want to keep the game world, so doing nothing here.
        return
    }

    @Transactional
    fun handleGameWorldStatusChangedEvent(event: GameWorldStatusChangedEvent) {
        val world = worldRepository.getWorldById(event.gameWorldId)
        if (world != null) {
            world.status = event.status
            worldRepository.save(world)
        }
    }

    @Transactional
    fun handleResourceMinedEvent(event: ResourceMinedEvent) {
        val planet = planetRepository.getPlanetById(event.planetId)
        planet?.run {
            resources[event.resource.resourceType]?.let {
                it.currentAmount -= event.minedAmount
            }
            planetRepository.save(this)
        }
    }

}