package io.archilab.themicroservicedungeon.gamelog.domain.eventhandlers

import io.archilab.themicroservicedungeon.gamelog.domain.entities.Game
import io.archilab.themicroservicedungeon.gamelog.domain.entities.Player
import io.archilab.themicroservicedungeon.gamelog.domain.enums.GameStatus
import io.archilab.themicroservicedungeon.gamelog.domain.events.game.PlayerStatusEvent
import io.archilab.themicroservicedungeon.gamelog.domain.repositories.GameRepository
import io.archilab.themicroservicedungeon.gamelog.domain.repositories.PlayerRepository
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional

@Component
class PlayerStatusEventHandler {

    @Autowired
    private lateinit var playerRepository: PlayerRepository

    @Autowired
    private lateinit var gameRepository: GameRepository

    private val logger = LoggerFactory.getLogger(PlayerStatusEventHandler::class.java)

    @Transactional
    fun handlePlayerStatusEvent(event: PlayerStatusEvent) {
        val player = playerRepository.getPlayerById(event.playerId)?.apply {
            name = event.name
        } ?: Player(event.playerId, event.name)
        val game = gameRepository.getGameById(event.gameId) ?: Game(
            event.gameId,
            null,
            GameStatus.CREATED
        ).also {
            gameRepository.saveGame(it)
            logger.warn("Game ${it.id} doesn't exist and is now auto-created. ")
        }
        player.apply {
            player.joinGame(game)
            playerRepository.savePlayer(this)
        }
        game.apply {
            gameRepository.saveGame(this)
        }
    }

}