package io.archilab.themicroservicedungeon.gamelog.domain.events.game

import com.fasterxml.jackson.annotation.JacksonInject
import com.fasterxml.jackson.annotation.JsonProperty
import io.archilab.themicroservicedungeon.gamelog.domain.enums.GameStatus
import io.archilab.themicroservicedungeon.gamelog.domain.events.InternalEvent
import java.time.Instant
import java.util.*

data class GameStatusEvent (
    val gameId: UUID,
    @JsonProperty("gameworldId")
    val gameWorldId: UUID?,
    val status: GameStatus,
    @JacksonInject("timestamp")
    val timestamp: Instant
) : InternalEvent