package io.archilab.themicroservicedungeon.gamelog.domain.events.game

import com.fasterxml.jackson.annotation.JacksonInject
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import io.archilab.themicroservicedungeon.gamelog.domain.enums.RoundStatus
import io.archilab.themicroservicedungeon.gamelog.domain.events.InternalEvent
import java.time.Instant
import java.util.UUID

@JsonIgnoreProperties(value = ["impreciseTimings", "impreciseTimingPredictions"])
data class RoundStatusEvent(
    val gameId: UUID,
    val roundId: UUID,
    val roundNumber: Int,
    val roundStatus: RoundStatus,
    @JacksonInject("timestamp")
    val timestamp: Instant
) : InternalEvent