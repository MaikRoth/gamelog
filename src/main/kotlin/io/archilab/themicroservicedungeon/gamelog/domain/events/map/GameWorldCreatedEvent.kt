package io.archilab.themicroservicedungeon.gamelog.domain.events.map

import com.fasterxml.jackson.annotation.JsonProperty
import io.archilab.themicroservicedungeon.gamelog.domain.enums.WorldStatus
import io.archilab.themicroservicedungeon.gamelog.domain.events.InternalEvent
import io.archilab.themicroservicedungeon.gamelog.domain.events.map.embedded.GameWorldPlanet
import java.util.*

data class GameWorldCreatedEvent(
    @JsonProperty("id")
    val gameWorldId: UUID,
    val status: WorldStatus,
    val planets: List<GameWorldPlanet>
) : InternalEvent
