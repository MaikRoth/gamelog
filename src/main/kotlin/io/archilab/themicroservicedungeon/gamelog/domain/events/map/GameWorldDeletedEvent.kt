package io.archilab.themicroservicedungeon.gamelog.domain.events.map

import com.fasterxml.jackson.annotation.JsonProperty
import io.archilab.themicroservicedungeon.gamelog.domain.events.InternalEvent
import java.util.UUID

data class GameWorldDeletedEvent(
    @JsonProperty("id")
    val gameWorldId: UUID
) : InternalEvent