package io.archilab.themicroservicedungeon.gamelog.domain.events.map

import com.fasterxml.jackson.annotation.JsonProperty
import io.archilab.themicroservicedungeon.gamelog.domain.enums.WorldStatus
import io.archilab.themicroservicedungeon.gamelog.domain.events.InternalEvent
import java.util.*

data class GameWorldStatusChangedEvent(
    @JsonProperty("id")
    val gameWorldId: UUID,
    val status: WorldStatus
) : InternalEvent