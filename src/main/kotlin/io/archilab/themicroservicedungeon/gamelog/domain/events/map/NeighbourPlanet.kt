package io.archilab.themicroservicedungeon.gamelog.domain.events.map

import com.fasterxml.jackson.annotation.JsonProperty
import io.archilab.themicroservicedungeon.gamelog.domain.enums.Direction
import java.util.*

data class NeighbourPlanet(
    @JsonProperty("id")
    val planetId: UUID,
    val direction: Direction
)