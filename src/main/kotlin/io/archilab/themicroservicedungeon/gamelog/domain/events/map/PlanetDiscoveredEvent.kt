package io.archilab.themicroservicedungeon.gamelog.domain.events.map

import com.fasterxml.jackson.annotation.JsonProperty
import io.archilab.themicroservicedungeon.gamelog.domain.events.InternalEvent
import io.archilab.themicroservicedungeon.gamelog.domain.events.map.embedded.GameWorldResource
import java.util.*

data class PlanetDiscoveredEvent(
    @JsonProperty("planet")
    val planetId: UUID,
    val movementDifficulty: Int,
    val neighbours: List<NeighbourPlanet>,
    val resource: GameWorldResource?
) : InternalEvent