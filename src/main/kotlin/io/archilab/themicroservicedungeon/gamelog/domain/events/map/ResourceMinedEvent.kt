package io.archilab.themicroservicedungeon.gamelog.domain.events.map

import com.fasterxml.jackson.annotation.JsonProperty
import io.archilab.themicroservicedungeon.gamelog.domain.events.InternalEvent
import io.archilab.themicroservicedungeon.gamelog.domain.events.map.embedded.GameWorldResource
import java.util.*

data class ResourceMinedEvent(
    @JsonProperty("planet")
    val planetId: UUID,
    val minedAmount: Int,
    val resource: GameWorldResource
) : InternalEvent