package io.archilab.themicroservicedungeon.gamelog.domain.events.map.embedded

import com.fasterxml.jackson.annotation.JsonAlias
import io.archilab.themicroservicedungeon.gamelog.domain.enums.ResourceType

data class GameWorldResource(
    @JsonAlias("type")
    val resourceType: ResourceType,
    val maxAmount: Int,
    val currentAmount: Int
)