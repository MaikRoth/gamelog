package io.archilab.themicroservicedungeon.gamelog.domain.events.robot

import com.fasterxml.jackson.annotation.JacksonInject
import com.fasterxml.jackson.annotation.JsonProperty
import io.archilab.themicroservicedungeon.gamelog.domain.events.InternalEvent
import io.archilab.themicroservicedungeon.gamelog.domain.events.robot.embedded.FightingRobotProperties
import java.time.Instant

data class RobotAttackedEvent(
    @JsonProperty("attacker")
    val attacker: FightingRobotProperties,
    @JsonProperty("target")
    val defender: FightingRobotProperties,
    @JacksonInject("timestamp")
    val timestamp: Instant
) : InternalEvent
