package io.archilab.themicroservicedungeon.gamelog.domain.events.robot

import com.fasterxml.jackson.annotation.JacksonInject
import com.fasterxml.jackson.annotation.JsonProperty
import io.archilab.themicroservicedungeon.gamelog.domain.events.InternalEvent
import io.archilab.themicroservicedungeon.gamelog.domain.events.robot.embedded.MovementPlanetProperties
import java.time.Instant
import java.util.*

data class RobotMovedEvent (
    @JsonProperty("robotId")
    val robotId: UUID,
    @JsonProperty("remainingEnergy")
    val remainingEnergy: Int,
    @JsonProperty("fromPlanet")
    val originPlanet: MovementPlanetProperties,
    @JsonProperty("toPlanet")
    val destinationPlanet: MovementPlanetProperties,
    @JacksonInject("timestamp")
    val timestamp: Instant
        ) : InternalEvent
