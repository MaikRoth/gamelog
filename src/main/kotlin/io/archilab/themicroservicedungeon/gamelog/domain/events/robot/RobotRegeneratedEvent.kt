package io.archilab.themicroservicedungeon.gamelog.domain.events.robot

import com.fasterxml.jackson.annotation.JacksonInject
import com.fasterxml.jackson.annotation.JsonProperty
import io.archilab.themicroservicedungeon.gamelog.domain.events.InternalEvent
import java.time.Instant
import java.util.*

data class RobotRegeneratedEvent(
    @JsonProperty("robotId")
    val robotId: UUID,
    @JsonProperty("availableEnergy")
    val availableEnergy: Int,
    @JacksonInject("timestamp")
    val timestamp: Instant
) : InternalEvent
