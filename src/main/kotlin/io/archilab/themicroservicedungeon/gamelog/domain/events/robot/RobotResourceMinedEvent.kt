package io.archilab.themicroservicedungeon.gamelog.domain.events.robot

import com.fasterxml.jackson.annotation.JacksonInject
import com.fasterxml.jackson.annotation.JsonProperty
import io.archilab.themicroservicedungeon.gamelog.domain.enums.ResourceType
import io.archilab.themicroservicedungeon.gamelog.domain.events.InternalEvent
import io.archilab.themicroservicedungeon.gamelog.domain.events.robot.embedded.ResourceInventory
import java.time.Instant
import java.util.*

data class RobotResourceMinedEvent(
    @JsonProperty("robotId")
    val robotId: UUID,
    @JsonProperty("minedAmount")
    val minedAmount: Int,
    @JsonProperty("minedResource")
    val minedResource: ResourceType,
    @JsonProperty("resourceInventory")
    val resourceInventory: ResourceInventory,
    @JacksonInject("timestamp")
    val timestamp: Instant
) : InternalEvent
