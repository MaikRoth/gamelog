package io.archilab.themicroservicedungeon.gamelog.domain.events.robot

import com.fasterxml.jackson.annotation.JacksonInject
import com.fasterxml.jackson.annotation.JsonProperty
import io.archilab.themicroservicedungeon.gamelog.domain.enums.ResourceType
import io.archilab.themicroservicedungeon.gamelog.domain.events.InternalEvent
import io.archilab.themicroservicedungeon.gamelog.domain.events.robot.embedded.ResourceInventory
import java.time.Instant
import java.util.*

data class RobotResourceRemovedEvent(
    @JsonProperty("robotId")
    val robotId: UUID,
    @JsonProperty("removedAmount")
    val minedAmount: Int,
    @JsonProperty("removedResource")
    val minedResource: ResourceType,
    @JsonProperty("resourceInventory")
    val resourceInventory: ResourceInventory,
    @JacksonInject("timestamp")
    val timestamp: Instant
) : InternalEvent
