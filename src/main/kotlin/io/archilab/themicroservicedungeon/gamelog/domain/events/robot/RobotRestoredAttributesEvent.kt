package io.archilab.themicroservicedungeon.gamelog.domain.events.robot

import com.fasterxml.jackson.annotation.JsonProperty
import io.archilab.themicroservicedungeon.gamelog.domain.enums.RestorationType
import io.archilab.themicroservicedungeon.gamelog.domain.events.InternalEvent
import java.util.*

data class RobotRestoredAttributesEvent(
    @JsonProperty("robotId")
    val robotId: UUID,
    @JsonProperty("restorationType")
    val restorationType: RestorationType,
    @JsonProperty("availableEnergy")
    val availableEnergy: Int,
    @JsonProperty("availableHealth")
    val availableHealth: Int
): InternalEvent
