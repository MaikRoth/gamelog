import io.archilab.themicroservicedungeon.gamelog.domain.events.InternalEvent
import io.archilab.themicroservicedungeon.gamelog.domain.events.robot.embedded.RobotProperties

data class RobotSpawnedEvent(
    val robot: RobotProperties
) : InternalEvent
