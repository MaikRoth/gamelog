package io.archilab.themicroservicedungeon.gamelog.domain.events.robot

import com.fasterxml.jackson.annotation.JsonProperty
import io.archilab.themicroservicedungeon.gamelog.domain.enums.UpgradeType
import io.archilab.themicroservicedungeon.gamelog.domain.events.InternalEvent
import io.archilab.themicroservicedungeon.gamelog.domain.events.robot.embedded.RobotProperties
import java.util.*

data class RobotUpgradedEvent (
    @JsonProperty("robotId")
    val robotId: UUID,
    @JsonProperty("level")
    val level: Int,
    @JsonProperty("upgrade")
    val upgrade: UpgradeType,
    @JsonProperty("robot")
    val robot: RobotProperties
) : InternalEvent
