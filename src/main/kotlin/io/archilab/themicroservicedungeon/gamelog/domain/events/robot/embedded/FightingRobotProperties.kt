package io.archilab.themicroservicedungeon.gamelog.domain.events.robot.embedded

import com.fasterxml.jackson.annotation.JsonProperty
import java.util.*

data class FightingRobotProperties (
    @JsonProperty("robotId")
    val id: UUID,
    @JsonProperty("availableHealth")
    val health: Int,
    @JsonProperty("availableEnergy")
    val energy: Int,
    @JsonProperty("alive")
    val alive: Boolean
)