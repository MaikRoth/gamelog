package io.archilab.themicroservicedungeon.gamelog.domain.events.robot.embedded

import com.fasterxml.jackson.annotation.JsonProperty
import java.util.*

data class MovementPlanetProperties (
    @JsonProperty("id")
    val planetId: UUID,
    @JsonProperty("movementDifficulty")
    val movementDifficulty: Int
)