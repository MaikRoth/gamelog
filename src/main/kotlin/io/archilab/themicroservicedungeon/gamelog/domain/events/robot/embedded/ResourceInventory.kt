package io.archilab.themicroservicedungeon.gamelog.domain.events.robot.embedded

import com.fasterxml.jackson.annotation.JsonProperty

data class ResourceInventory (
    @JsonProperty("COAL")
    val coal: Int,
    @JsonProperty("IRON")
    val iron: Int,
    @JsonProperty("GEM")
    val gem: Int,
    @JsonProperty("GOLD")
    val gold: Int,
    @JsonProperty("PLATIN")
    val platin: Int
)