package io.archilab.themicroservicedungeon.gamelog.domain.events.robot.embedded

import com.fasterxml.jackson.annotation.JsonProperty
import java.util.*

data class RobotProperties (
    @JsonProperty("id")
    val robotId: UUID,
    val alive: Boolean,
    @JsonProperty("player")
    val playerId: UUID,
    val maxHealth: Int,
    val maxEnergy: Int,
    @JsonProperty("energyRegen")
    val energyRegeneration: Int,
    val attackDamage: Int,
    val miningSpeed: Int,
    val health: Int,
    val energy: Int,
    val healthLevel: Int,
    val damageLevel: Int,
    val miningSpeedLevel: Int,
    val miningLevel: Int,
    val energyLevel: Int,
    @JsonProperty("energyRegenLevel")
    val energyRegenerationLevel: Int,
    val planet: Planet,
    val inventory: Inventory,
)