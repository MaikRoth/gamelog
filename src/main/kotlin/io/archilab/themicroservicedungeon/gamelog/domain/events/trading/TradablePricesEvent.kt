package io.archilab.themicroservicedungeon.gamelog.domain.events.trading

import io.archilab.themicroservicedungeon.gamelog.domain.events.InternalEvent
import io.archilab.themicroservicedungeon.gamelog.domain.events.trading.embedded.TradablePrice

class TradablePricesEvent() : ArrayList<TradablePrice>(), InternalEvent