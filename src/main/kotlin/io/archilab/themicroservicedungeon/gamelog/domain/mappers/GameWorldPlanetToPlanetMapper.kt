package io.archilab.themicroservicedungeon.gamelog.domain.mappers

import io.archilab.themicroservicedungeon.gamelog.domain.entities.Planet
import io.archilab.themicroservicedungeon.gamelog.domain.entities.Resource
import io.archilab.themicroservicedungeon.gamelog.domain.events.map.embedded.GameWorldPlanet
import org.springframework.stereotype.Component

@Component
class GameWorldPlanetToPlanetMapper() {

    fun mapList(gameWorldPlanets: List<GameWorldPlanet>) : List<Planet> {
        val planets = arrayListOf<Planet>()
        for (gameWorldPlanet in gameWorldPlanets) {
            val planet = mapItem(gameWorldPlanet)
            planets.add(planet)
        }
        return planets
    }

    fun mapItem(gameWorldPlanet: GameWorldPlanet): Planet {
        val resource = gameWorldPlanet.resource?.let {
            Resource(it.resourceType, it.maxAmount, it.currentAmount)
        }
        val resources = resource?.let {
            mapOf(Pair(resource.resourceType, resource))
        }?: mapOf()
        return Planet(
            gameWorldPlanet.planetId,
            gameWorldPlanet.x,
            gameWorldPlanet.y,
            gameWorldPlanet.movementDifficulty,
            resources
        )
    }

}