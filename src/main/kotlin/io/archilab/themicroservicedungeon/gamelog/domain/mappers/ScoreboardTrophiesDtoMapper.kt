package io.archilab.themicroservicedungeon.gamelog.domain.mappers

import io.archilab.themicroservicedungeon.gamelog.domain.dtos.PlayerDto
import io.archilab.themicroservicedungeon.gamelog.domain.dtos.PlayerScoreboardTrophiesDto
import io.archilab.themicroservicedungeon.gamelog.domain.dtos.ScoreboardTrophiesDto
import io.archilab.themicroservicedungeon.gamelog.domain.dtos.ScoreboardTrophyDto
import io.archilab.themicroservicedungeon.gamelog.domain.entities.Game
import io.archilab.themicroservicedungeon.gamelog.domain.entities.Player
import io.archilab.themicroservicedungeon.gamelog.domain.trophies.TrophiesManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import java.util.*

@Component
class ScoreboardTrophiesDtoMapper {

    @Autowired
    private lateinit var trophiesManager: TrophiesManager

    fun mapGameScoreboardToScoreboardTrophiesDto(game: Game?): ScoreboardTrophiesDto {
        if (game == null) return ScoreboardTrophiesDto(null, listOf())
        return ScoreboardTrophiesDto(
            game.id,
            game.players.map { player ->
                getPlayerScoreboardTrophiesDtoFromPlayer(game.id, player)
            }
        )
    }

    private fun getPlayerScoreboardTrophiesDtoFromPlayer(gameId: UUID, player: Player): PlayerScoreboardTrophiesDto {
        return PlayerScoreboardTrophiesDto(
            PlayerDto(player.id, player.name),
            player.trophies
                .filter { playerTrophy -> playerTrophy.gameId == gameId }
                .map { playerTrophy ->
                ScoreboardTrophyDto(
                    playerTrophy.trophy.name,
                    playerTrophy.trophy.imageUrl,
                    trophiesManager.getScoreboardCategory(playerTrophy.trophy.id)!!
                )
            },
            gameId
        )
    }

}