package io.archilab.themicroservicedungeon.gamelog.domain.repositories

import io.archilab.themicroservicedungeon.gamelog.domain.dtos.AchievementsDto

interface AchievementDtosRepository {
    fun getAchievements(): AchievementsDto
}