package io.archilab.themicroservicedungeon.gamelog.domain.repositories

import io.archilab.themicroservicedungeon.gamelog.domain.dtos.MapDto

interface MapRepository {

    fun getMap() : MapDto?

}