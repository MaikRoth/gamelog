package io.archilab.themicroservicedungeon.gamelog.domain.repositories

import io.archilab.themicroservicedungeon.gamelog.domain.entities.Planet
import java.util.UUID

interface PlanetRepository {

    fun getPlanetById(planetId: UUID) : Planet?

    fun save(planet: Planet)

}