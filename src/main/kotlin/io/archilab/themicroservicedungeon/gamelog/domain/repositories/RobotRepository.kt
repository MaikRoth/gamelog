package io.archilab.themicroservicedungeon.gamelog.domain.repositories

import io.archilab.themicroservicedungeon.gamelog.domain.entities.Robot
import java.util.UUID

interface RobotRepository {

    fun getRobotById(robotId: UUID) : Robot?

    fun getRobotsForPlanet(planetId: UUID) : List<Robot>

    fun save(robot: Robot)

}