package io.archilab.themicroservicedungeon.gamelog.domain.repositories

import io.archilab.themicroservicedungeon.gamelog.domain.dtos.ScoreboardDto

interface ScoreboardRepository {

    fun getScoreboard() : ScoreboardDto?

}