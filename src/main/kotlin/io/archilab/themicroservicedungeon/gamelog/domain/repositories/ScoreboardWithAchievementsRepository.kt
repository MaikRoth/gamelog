package io.archilab.themicroservicedungeon.gamelog.domain.repositories

import io.archilab.themicroservicedungeon.gamelog.domain.dtos.ScoreboardWithAchievementsDto

interface ScoreboardWithAchievementsRepository {

    fun getScoreboardWithAchievements(): ScoreboardWithAchievementsDto

}