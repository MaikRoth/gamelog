package io.archilab.themicroservicedungeon.gamelog.domain.scoreboard

import io.archilab.themicroservicedungeon.gamelog.domain.entities.Game
import io.archilab.themicroservicedungeon.gamelog.domain.entities.Player
import io.archilab.themicroservicedungeon.gamelog.domain.entities.PlayerScore
import io.archilab.themicroservicedungeon.gamelog.domain.entities.Scoreboard
import org.springframework.stereotype.Component
import java.util.*

@Component
class ScoreboardStrategy {

    private final val pointsForFight = 5
    private final val pointsForKill = 10

    private final val pointsPerCredit = 0.01.toBigDecimal()

    private final val coalWeight = 1
    private final val ironWeight = 2
    private final val gemWeight = 3
    private final val goldWeight = 4
    private final val platinWeight = 5

    private final val travelingScoreWeight = 1.0
    private final val miningScoreWeight = 1.25
    private final val tradingScoreWeight = 1.5
    private final val fightingScoreWeight = 1.75

    fun calculateScoreboard(game: Game, roundNumber: Int): Scoreboard {
        val playerScores = game.players.map {
            val fightingScore = calculateFightingScore(it, game.id)
            val miningScore = calculateMiningScore(it, game.id)
            val tradingScore = calculateTradingScore(it, game.id)
            val travelingScore = calculateTravelingScore(it, game.id)
            val totalScore = calculateTotalScore(fightingScore, miningScore, tradingScore, travelingScore)
            PlayerScore(
                it,
                totalScore,
                fightingScore,
                miningScore,
                tradingScore,
                travelingScore
            )
        }
        return Scoreboard(game.id, roundNumber, playerScores)
    }

    private fun calculateMiningScore(player: Player, gameId: UUID): Int {
        return player.robots.filter{ robot -> robot.gameId == gameId }.sumOf { robot ->
            with(robot.minedResources) {
                coalWeight * coal + ironWeight * iron + gemWeight * gem + goldWeight * gold + platinWeight * platin
            }
        }
    }

    private fun calculateFightingScore(player: Player, gameId: UUID): Int {
        return player.robots.filter{ robot -> robot.gameId == gameId }.sumOf { robot ->
            robot.offensiveFights.count() * pointsForFight + robot.kills.count() * pointsForKill
        }
    }

    private fun calculateTradingScore(player: Player, gameId: UUID): Int {
        return player.tradingTransactions.filter { it.gameId == gameId }.sumOf{
            it.totalPrice * pointsPerCredit
        }.toInt()
    }

    private fun calculateTravelingScore(player: Player, gameId: UUID): Int {
        return player.robots.filter{ robot -> robot.gameId == gameId }.sumOf {
            robot -> robot.movements.count()
        }
    }

    private fun calculateTotalScore(fightingScore: Int, miningScore: Int, tradingScore: Int, travelingScore: Int): Int {
        return (travelingScore * travelingScoreWeight + miningScore * miningScoreWeight + tradingScore * tradingScoreWeight + fightingScore * fightingScoreWeight).toInt()
    }

}