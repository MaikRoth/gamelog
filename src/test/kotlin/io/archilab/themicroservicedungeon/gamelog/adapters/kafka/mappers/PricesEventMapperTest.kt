package io.archilab.themicroservicedungeon.gamelog.adapters.kafka.mappers

import TestWithH2AndWithoutKafka
import io.archilab.themicroservicedungeon.gamelog.domain.exceptions.MapperUnknownEventTypeException
import io.archilab.themicroservicedungeon.gamelog.domain.events.trading.embedded.TradablePrice
import io.archilab.themicroservicedungeon.gamelog.domain.enums.TradableType
import io.archilab.themicroservicedungeon.gamelog.domain.events.trading.TradablePricesEvent
import io.archilab.themicroservicedungeon.gamelog.adapters.kafka.mocks.EventMockFactory
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.springframework.boot.test.context.SpringBootTest
import java.math.BigDecimal

class PricesEventMapperTest : TestWithH2AndWithoutKafka() {

    private val eventMockFactory = EventMockFactory()
    private val mapper = PricesEventMapper()

    private val price = BigDecimal(5)
    private val name = "string"
    private val tradableType = TradableType.ITEM
    private val tradablePricesEvent = eventMockFactory.createTradablePricesEventMock(name, price, tradableType.type)

    @Test
    fun test_tradablePrices() {
        val expected = TradablePricesEvent()
        expected.add(TradablePrice(name, price, tradableType))
        val result = mapper.mapConsumerRecordToEvent(tradablePricesEvent.type, tradablePricesEvent.timestamp, tradablePricesEvent.payload)
        assertThat(result.javaClass).isEqualTo(TradablePricesEvent::class.java)
        assertThat(result).usingRecursiveComparison().isEqualTo(expected)
    }

    @Test
    fun test_exception() {
        assertThrows<MapperUnknownEventTypeException> {
            mapper.mapConsumerRecordToEvent("foobar", tradablePricesEvent.timestamp, tradablePricesEvent.payload)
        }
    }

}