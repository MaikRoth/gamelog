package io.archilab.themicroservicedungeon.gamelog.adapters.kafka.mappers

import TestWithH2AndWithoutKafka
import io.archilab.themicroservicedungeon.gamelog.domain.exceptions.MapperUnknownEventTypeException
import io.archilab.themicroservicedungeon.gamelog.domain.events.trading.TradableBoughtEvent
import io.archilab.themicroservicedungeon.gamelog.domain.enums.TradableType
import io.archilab.themicroservicedungeon.gamelog.adapters.kafka.mocks.EventMockFactory
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.springframework.boot.test.context.SpringBootTest
import java.math.BigDecimal
import java.util.*

class TradeBuyEventMapperTest : TestWithH2AndWithoutKafka() {

    private val eventMockFactory = EventMockFactory()
    private val mapper = TradeBuyEventMapper()

    private val playerId = UUID.randomUUID()
    private val robotId = UUID.randomUUID()
    private val type = TradableType.ITEM
    private val name = "somename"
    private val amount = 0
    private val pricePerUnit = BigDecimal(0)
    private val totalPrice = BigDecimal(0)
    private val tradableBoughtEvent = eventMockFactory.createTradableBoughtEventMock(
        playerId,
        robotId,
        type.type,
        name,
        amount,
        pricePerUnit,
        totalPrice
    )

    @Test
    fun test_tradableSold() {
        val expected = TradableBoughtEvent(playerId, robotId, type, name, amount, pricePerUnit, totalPrice)
        val result = mapper.mapConsumerRecordToEvent(tradableBoughtEvent.type, tradableBoughtEvent.timestamp, tradableBoughtEvent.payload)
        assertThat(result.javaClass).isEqualTo(TradableBoughtEvent::class.java)
        assertThat(result).usingRecursiveComparison().isEqualTo(expected)
    }

    @Test
    fun test_exception() {
        assertThrows<MapperUnknownEventTypeException> {
            mapper.mapConsumerRecordToEvent("foobar", tradableBoughtEvent.timestamp, tradableBoughtEvent.payload)
        }
    }

}