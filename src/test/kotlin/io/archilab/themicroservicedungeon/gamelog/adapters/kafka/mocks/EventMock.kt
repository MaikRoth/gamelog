package io.archilab.themicroservicedungeon.gamelog.adapters.kafka.mocks

import org.apache.kafka.clients.consumer.ConsumerRecord

/**
 * Collects the data required for the event listeners and mappers.
 */
data class EventMock (val type: String, val timestamp: String, val payload: ConsumerRecord<String, ByteArray>) {

    /**
     * Easy access to the JSON document in payload.value for manual comparison with the Async API specs.
     */
    val jsonBody: String
        get() {
            return String(payload.value())
        }

}