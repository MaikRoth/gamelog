package io.archilab.themicroservicedungeon.gamelog.adapters.kafka.mocks

import org.apache.kafka.clients.consumer.ConsumerRecord
import java.math.BigDecimal
import java.time.ZonedDateTime
import java.util.*

/**
 * Create EventMocks for testing of the event mappers and listeners.
 */
class EventMockFactory {

    fun createRoundStatusEventMock(gameId: UUID, roundId: UUID, roundNumber: Int, roundStatus: String): EventMock {
        val jsonPayload =
            """
                {
                  "gameId": "$gameId",
                  "roundId": "$roundId",
                  "roundNumber": $roundNumber,
                  "roundStatus": "$roundStatus"
                }
            """
        return createEventMockFromProperties("/roundStatus", "RoundStatus", jsonPayload, true)
    }

    fun createRoundStatusEventMockWithTimings(
        gameId: UUID,
        roundId: UUID,
        roundNumber: Int,
        roundStatus: String,
        timingRoundStarted: String,
        timingCommandInputEnded: String,
        timingRoundEnded: String,
        predictionRoundStart: String,
        predictionCommandInputEnd: String,
        predictionRoundEnd: String
    ): EventMock {
        val jsonPayload =
            """
                {
                  "gameId": "$gameId",
                  "roundId": "$roundId",
                  "roundNumber": $roundNumber,
                  "roundStatus": "$roundStatus",
                  "impreciseTimings": {
                    "roundStarted": "$timingRoundStarted",
                    "commandInputEnded": "$timingCommandInputEnded",
                    "roundEnded": "$timingRoundEnded"
                  },
                  "impreciseTimingPredictions": {
                    "roundStarted": "$predictionRoundStart",
                    "commandInputEnded": "$predictionCommandInputEnd",
                    "roundEnded": "$predictionRoundEnd"
                  }
                }
            """
        return createEventMockFromProperties("/roundStatus", "RoundStatus", jsonPayload, true)
    }

    fun createGameStatusEventMock(gameId: UUID, gameWorldId: UUID, status: String): EventMock {
        val jsonPayloadExample =
            """
                {
                  "gameId": "$gameId",
                  "gameworldId": "$gameWorldId",
                  "status": "$status"
                }
            """
        val type = "GameStatus"
        val topic = "/status"
        return createEventMockFromProperties(topic, type, jsonPayloadExample, true)
    }

    fun createGameStatusEventMockWithoutGameWorldId(gameId: UUID, status: String): EventMock {
        val jsonPayloadExample =
            """
                {
                  "gameId": "$gameId",
                  "status": "$status"
                }
            """
        val type = "GameStatus"
        val topic = "/status"
        return createEventMockFromProperties(topic, type, jsonPayloadExample, true)
    }

    fun createPlayerStatusEventMock(gameId: UUID, playerId: UUID, name: String): EventMock {
        val jsonPayloadExample =
            """
                {
                  "playerId": "$playerId",
                  "gameId": "$gameId",
                  "name": "$name"
                }
            """
        val type = "PlayerStatus"
        val topic = "/playerStatus"
        return createEventMockFromProperties(topic, type, jsonPayloadExample, true)
    }

    fun createGameWorldCreatedEventMock(
        gameWorldId: UUID,
        gameWorldStatus: String,
        planetId: UUID,
        defaultNumber: Int,
        resourceType: String
    ): EventMock {
        val jsonPayloadExample =
            """
                {
                  "id": "$gameWorldId",
                  "status": "$gameWorldStatus",
                  "planets": [
                    {
                      "id": "$planetId",
                      "x": $defaultNumber,
                      "y": $defaultNumber,
                      "movementDifficulty": $defaultNumber,
                      "resource": {
                        "resourceType": "$resourceType",
                        "maxAmount": $defaultNumber,
                        "currentAmount": $defaultNumber
                      }
                    }
                  ]
                }
            """
        val type = "GameworldCreated"
        val topic = "/gameworld"
        return createEventMockFromProperties(topic, type, jsonPayloadExample, false)
    }

    fun createGameWorldStatusChangedEventMock(
        gameWorldId: UUID,
        gameWorldStatus: String
    ): EventMock {
        val jsonPayloadExample =
            """
                {
                  "id": "$gameWorldId",
                  "status": "$gameWorldStatus"
                }
            """
        val type = "GameworldStatusChanged"
        val topic = "/gameworld"
        return createEventMockFromProperties(topic, type, jsonPayloadExample, false)
    }

    /**
     * Deprecated and not part of the new Map-Service written in Java.
     */
    fun createGameWorldDeletedEventMock(gameWorldId: UUID): EventMock {
        val jsonPayloadExample =
            """
                {
                  "id": "$gameWorldId"
                }
            """
        val type = "gameworld-deleted"
        val topic = "/gameworld"
        return createEventMockFromProperties(topic, type, jsonPayloadExample, false)
    }

    fun createResourceMinedEventMock(
        planetId: UUID,
        minedAmount: Int,
        minedResource: String,
        maxAmount: Int,
        currentAmount: Int
    ): EventMock {
        val jsonPayloadExample =
            """
                {
                  "planet": "$planetId",
                  "minedAmount": $minedAmount,
                  "resource": {
                    "type": "$minedResource",
                    "maxAmount": $maxAmount,
                    "currentAmount": $currentAmount
                  }
                }
            """
        val type = "ResourceMined"
        val topic = "/planet"
        return createEventMockFromProperties(topic, type, jsonPayloadExample, false)
    }

    fun createPlanetDiscoveredEventMock(
        planetId: UUID,
        movementDifficulty: Int,
        neighbourPlanetId: UUID,
        neighbourDirection: String,
        resourceMaxAmount: Int,
        resourceCurrentAmount: Int,
        resourceType: String
    ): EventMock {
        val jsonPayloadExample =
            """
                {
                  "planet": "$planetId",
                  "movementDifficulty": $movementDifficulty,
                  "neighbours": [
                    {
                      "id": "$neighbourPlanetId",
                      "direction": "$neighbourDirection"
                    }
                  ],
                  "resource": {
                    "resourceType": "$resourceType",
                    "maxAmount": $resourceMaxAmount,
                    "currentAmount": $resourceCurrentAmount
                  }
                }
            """
        val type = "PlanetDiscovered"
        val topic = "/planet"
        return createEventMockFromProperties(topic, type, jsonPayloadExample, false)
    }

    fun createRobotAttackedEvent(
        attackerId: UUID,
        attackerHealth: Int,
        attackerEnergy: Int,
        attackerAlive: Boolean,
        targetId: UUID,
        targetHealth: Int,
        targetEnergy: Int,
        targetAlive: Boolean
    ): EventMock {
        val jsonPayloadExample =
            """
                {
                  "attacker": {
                    "robotId": "$attackerId",
                    "availableHealth": $attackerHealth,
                    "availableEnergy": $attackerEnergy,
                    "alive": $attackerAlive
                  },
                  "target": {
                    "robotId": "$targetId",
                    "availableHealth": $targetHealth,
                    "availableEnergy": $targetEnergy,
                    "alive": $targetAlive
                  }
                }
            """
        val type = "RobotAttackedEvent"
        val topic = "/robot"
        return createEventMockFromProperties(topic, type, jsonPayloadExample, true)
    }

    fun createRobotMovedEvent(
        robotId: UUID,
        energy: Int,
        originPlanetId: UUID,
        originMovementDifficulty: Int,
        destinationPlanetId: UUID,
        destinationMovementDifficulty: Int
    ): EventMock {
        val jsonPayloadExample =
            """
                {
                  "robotId": "$robotId",
                  "remainingEnergy": $energy,
                  "fromPlanet": {
                    "id": "$originPlanetId",
                    "movementDifficulty": $originMovementDifficulty
                  },
                  "toPlanet": {
                    "id": "$destinationPlanetId",
                    "movementDifficulty": $destinationMovementDifficulty
                  }
                }
            """
        val type = "RobotMovedEvent"
        val topic = "/robot"
        return createEventMockFromProperties(topic, type, jsonPayloadExample, true)
    }

    fun createRobotRegeneratedEvent(
        robotId: UUID,
        availableEnergy: Int
    ): EventMock {
        val jsonPayloadExample =
            """
                {
                  "robotId": "$robotId",
                  "availableEnergy": $availableEnergy
                }
            """
        val type = "RobotRegeneratedEvent"
        val topic = "/robot"
        return createEventMockFromProperties(topic, type, jsonPayloadExample, true)
    }

    fun createRobotResourceMinedEvent(
        robotId: UUID,
        minedAmount: Int,
        minedResource: String,
        coal: Int,
        iron: Int,
        gem: Int,
        gold: Int,
        platin: Int
    ): EventMock {
        val jsonPayloadExample =
            """
                {
                  "robotId": "$robotId",
                  "minedAmount": $minedAmount,
                  "minedResource": "$minedResource",
                  "resourceInventory": {
                    "COAL": $coal,
                    "IRON": $iron,
                    "GEM": $gem,
                    "GOLD": $gold,
                    "PLATIN": $platin
                  }
                }
            """
        val type = "RobotResourceMinedEvent"
        val topic = "/robot"
        return createEventMockFromProperties(topic, type, jsonPayloadExample, true)
    }

    fun createRobotResourceRemovedEvent(
        robotId: UUID,
        removedAmount: Int,
        removedResource: String,
        coal: Int,
        iron: Int,
        gem: Int,
        gold: Int,
        platin: Int
    ): EventMock {
        val jsonPayloadExample =
            """
                {
                  "robotId": "$robotId",
                  "removedAmount": $removedAmount,
                  "removedResource": "$removedResource",
                  "resourceInventory": {
                    "COAL": $coal,
                    "IRON": $iron,
                    "GEM": $gem,
                    "GOLD": $gold,
                    "PLATIN": $platin
                  }
                }
            """
        val type = "RobotResourceRemovedEvent"
        val topic = "/robot"
        return createEventMockFromProperties(topic, type, jsonPayloadExample, true)
    }

    fun createRobotRestoredAttributesEvent(
        robotId: UUID,
        restorationType: String,
        availableEnergy: Int,
        availableHealth: Int
    ): EventMock {
        val jsonPayloadExample =
            """
                {
                  "robotId": "$robotId",
                  "restorationType": "$restorationType",
                  "availableEnergy": $availableEnergy,
                  "availableHealth": $availableHealth
                }
            """
        val type = "RobotRestoredAttributesEvent"
        val topic = "/robot"
        return createEventMockFromProperties(topic, type, jsonPayloadExample, true)
    }

    fun createRobotSpawnedEvent(
        robotId: UUID,
        alive: Boolean,
        playerId: UUID,
        maxHealth: Int,
        maxEnergy: Int,
        energyRegeneration: Int,
        attackDamage: Int,
        miningSpeed: Int,
        health: Int,
        energy: Int,
        healthLevel: Int,
        damageLevel: Int,
        miningSpeedLevel: Int,
        miningLevel: Int,
        energyLevel: Int,
        energyRegenerationLevel: Int,
        planet: MockPlanet,
        inventory: MockRobotInventory
    ): EventMock {
        val jsonPayloadExample =
            """
                {
                  "robot": {
                    "planet": {
                      "planetId": "${planet.planetId}",
                      "gameWorldId": "${planet.gameWorldId}",
                      "movementDifficulty": ${planet.movementDifficulty},
                      "resourceType": "${planet.resourceType}"
                    },
                    "inventory": {
                      "storageLevel": ${inventory.storageLevel},
                      "usedStorage": ${inventory.usedStorage},
                      "maxStorage": ${inventory.maxStorage},
                      "full": ${inventory.full},
                      "resources": {
                        "COAL": ${inventory.resources.coal},
                        "IRON": ${inventory.resources.iron},
                        "GEM": ${inventory.resources.gem},
                        "GOLD": ${inventory.resources.gold},
                        "PLATIN":${inventory.resources.platin}
                      }
                    },
                    "id": "$robotId",
                    "alive": $alive,
                    "player": "$playerId",
                    "maxHealth": $maxHealth,
                    "maxEnergy": $maxEnergy,
                    "energyRegen": $energyRegeneration,
                    "attackDamage": $attackDamage,
                    "miningSpeed": $miningSpeed,
                    "health": $health,
                    "energy": $energy,
                    "healthLevel": $healthLevel,
                    "damageLevel": $damageLevel,
                    "miningSpeedLevel": $miningSpeedLevel,
                    "miningLevel": $miningLevel,
                    "energyLevel": $energyLevel,
                    "energyRegenLevel": $energyRegenerationLevel
                  }
                }
            """
        val type = "RobotSpawnedEvent"
        val topic = "/robot"
        return createEventMockFromProperties(topic, type, jsonPayloadExample, true)
    }

    fun createRobotUpgradedEvent(
        robotId: UUID, level: Int, upgrade: String,
        alive: Boolean,
        playerId: UUID,
        maxHealth: Int,
        maxEnergy: Int,
        energyRegeneration: Int,
        attackDamage: Int,
        miningSpeed: Int,
        health: Int,
        energy: Int,
        healthLevel: Int,
        damageLevel: Int,
        miningSpeedLevel: Int,
        miningLevel: Int,
        energyLevel: Int,
        energyRegenerationLevel: Int,
        planet: MockPlanet,
        inventory: MockRobotInventory
    ): EventMock {
        val jsonPayloadExample =
            """
                {
                  "robotId": "$robotId",
                  "level": $level,
                  "upgrade": "$upgrade",
                  "robot": {
                    "planet": {
                      "planetId": "${planet.planetId}",
                      "gameWorldId": "${planet.gameWorldId}",
                      "movementDifficulty": ${planet.movementDifficulty},
                      "resourceType": "${planet.resourceType}"
                    },
                    "inventory": {
                      "storageLevel": ${inventory.storageLevel},
                      "usedStorage": ${inventory.usedStorage},
                      "maxStorage": ${inventory.maxStorage},
                      "full": ${inventory.full},
                      "resources": {
                        "COAL": ${inventory.resources.coal},
                        "IRON": ${inventory.resources.iron},
                        "GEM": ${inventory.resources.gem},
                        "GOLD": ${inventory.resources.gold},
                        "PLATIN":${inventory.resources.platin}
                      }
                    },
                    "id": "$robotId",
                    "alive": $alive,
                    "player": "$playerId",
                    "maxHealth": $maxHealth,
                    "maxEnergy": $maxEnergy,
                    "energyRegen": $energyRegeneration,
                    "attackDamage": $attackDamage,
                    "miningSpeed": $miningSpeed,
                    "health": $health,
                    "energy": $energy,
                    "healthLevel": $healthLevel,
                    "damageLevel": $damageLevel,
                    "miningSpeedLevel": $miningSpeedLevel,
                    "miningLevel": $miningLevel,
                    "energyLevel": $energyLevel,
                    "energyRegenLevel": $energyRegenerationLevel
                  }
                }
            """
        val type = "RobotUpgradedEvent"
        val topic = "/robot"
        return createEventMockFromProperties(topic, type, jsonPayloadExample, true)
    }

    fun createRobotsRevealedEvent(
        revealedRobotId: UUID,
        planetId: UUID,
        playerNotion: String,
        health: Int,
        energy: Int,
        healthLevel: Int,
        damageLevel: Int,
        miningSpeedLevel: Int,
        miningLevel: Int,
        energyLevel: Int,
        energyRegenLevel: Int,
        storageLevel: Int
    ): EventMock {
        val jsonPayloadExample =
            """
                {
                  "robots": [
                    {
                      "robotId": "$revealedRobotId",
                      "planetId": "$planetId",
                      "playerNotion": "$playerNotion",
                      "health": $health,
                      "energy": $energy,
                      "levels": {
                        "healthLevel": $healthLevel,
                        "damageLevel": $damageLevel,
                        "miningSpeedLevel": $miningSpeedLevel,
                        "miningLevel": $miningLevel,
                        "energyLevel": $energyLevel,
                        "energyRegenLevel": $energyRegenLevel,
                        "storageLevel": $storageLevel
                      }
                    }
                  ]
                }
            """
        val type = "RobotsRevealedEvent"
        val topic = "/robot"
        return createEventMockFromProperties(topic, type, jsonPayloadExample, true)
    }

    fun createBankAccountClearedEventMock(playerId: UUID, balance: Int): EventMock {
        val jsonPayloadExample =
            """
                {
                  "playerId": "$playerId",
                  "balance": $balance
                }
            """
        val type = "BankAccountCleared"
        val topic = "/bank"
        return createEventMockFromProperties(topic, type, jsonPayloadExample, true)
    }

    fun createBankAccountInitializedEventMock(playerId: UUID, balance: Int): EventMock {
        val jsonPayloadExample =
            """
                {
                  "playerId": "$playerId",
                  "balance": $balance
                }
            """
        val type = "BankAccountInitialized"
        val topic = "/bank"
        return createEventMockFromProperties(topic, type, jsonPayloadExample, true)
    }

    fun createBankAccountTransactionBookedEventMock(playerId: UUID, transactionAmount: Int, balance: Int): EventMock {
        val jsonPayloadExample =
            """
                {
                  "playerId": "$playerId",
                  "transactionAmount": $transactionAmount,
                  "balance": $balance
                }
            """
        val type = "BankAccountTransactionBooked"
        val topic = "/bank"
        return createEventMockFromProperties(topic, type, jsonPayloadExample, true)
    }

    fun createTradablePricesEventMock(name: String, price: BigDecimal, tradableType: String): EventMock {
        val jsonPayloadExample =
            """
                [
                  {
                    "name": "$name",
                    "price": $price,
                    "type": "$tradableType"
                  }
                ]
            """
        val type = "TradablePrices"
        val topic = "/prices"
        return createEventMockFromProperties(topic, type, jsonPayloadExample, true)
    }

    fun createTradableSoldEventMock(
        playerId: UUID,
        robotId: UUID,
        tradableType: String,
        name: String,
        amount: Int,
        pricePerUnit: BigDecimal,
        totalPrice: BigDecimal
    ): EventMock {
        val jsonPayloadExample =
            """
                {
                    "playerId": "$playerId",
                    "robotId": "$robotId",
                    "type": "$tradableType",
                    "name": "$name",
                    "amount": $amount,
                    "pricePerUnit": $pricePerUnit,
                    "totalPrice": $totalPrice
                }
            """
        val type = "TradableSold"
        val topic = "/trade-sell"
        return createEventMockFromProperties(topic, type, jsonPayloadExample, true)
    }

    fun createTradableBoughtEventMock(
        playerId: UUID,
        robotId: UUID,
        tradableType: String,
        name: String,
        amount: Int,
        pricePerUnit: BigDecimal,
        totalPrice: BigDecimal
    ): EventMock {
        val jsonPayloadExample =
            """
                {
                    "playerId": "$playerId",
                    "robotId": "$robotId",
                    "type": "$tradableType",
                    "name": "$name",
                    "amount": $amount,
                    "pricePerUnit": $pricePerUnit,
                    "totalPrice": $totalPrice
                }
            """
        val type = "TradableBought"
        val topic = "/trade-buy"
        return createEventMockFromProperties(topic, type, jsonPayloadExample, true)
    }

    private fun createEventMockFromProperties(
        topic: String,
        type: String,
        jsonPayload: String,
        hasTimestamp: Boolean
    ): EventMock {
        val value = jsonPayload.toByteArray()
        val payload = ConsumerRecord<String, ByteArray>(topic, 0, 0, null, value)
        val timestamp = if (hasTimestamp) generateTimestampString() else ""
        return EventMock(type, timestamp, payload)
    }

    companion object {
        fun generateTimestampString(): String {
            return ZonedDateTime.now().toInstant().toString()
        }
    }

}
