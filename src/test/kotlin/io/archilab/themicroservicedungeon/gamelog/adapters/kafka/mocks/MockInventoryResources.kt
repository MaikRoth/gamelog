package io.archilab.themicroservicedungeon.gamelog.adapters.kafka.mocks

data class MockInventoryResources (
    val coal: Int,
    val iron: Int,
    val gem: Int,
    val gold: Int,
    val platin: Int
)