package io.archilab.themicroservicedungeon.gamelog.adapters.kafka.mocks

import java.util.*

data class MockPlanet(
    val planetId: UUID,
    val gameWorldId: UUID,
    val movementDifficulty: Int,
    val resourceType: String
)