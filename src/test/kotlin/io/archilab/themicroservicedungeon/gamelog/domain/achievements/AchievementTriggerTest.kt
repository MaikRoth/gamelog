package io.archilab.themicroservicedungeon.gamelog.domain.achievements

import TestWithH2AndWithoutKafka
import io.archilab.themicroservicedungeon.gamelog.domain.entities.Game
import io.archilab.themicroservicedungeon.gamelog.domain.enums.AchievementType
import io.archilab.themicroservicedungeon.gamelog.domain.enums.GameStatus
import io.archilab.themicroservicedungeon.gamelog.domain.enums.RoundStatus
import io.archilab.themicroservicedungeon.gamelog.domain.eventhandlers.RoundStatusEventHandler
import io.archilab.themicroservicedungeon.gamelog.domain.events.game.RoundStatusEvent
import io.archilab.themicroservicedungeon.gamelog.domain.factories.PreconfiguredPlayersFactory
import io.archilab.themicroservicedungeon.gamelog.domain.repositories.GameRepository
import io.archilab.themicroservicedungeon.gamelog.domain.repositories.PlayerRepository
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.transaction.annotation.Transactional
import java.time.Instant
import java.util.*

@Transactional
class AchievementTriggerTest : TestWithH2AndWithoutKafka() {

    @Autowired
    private lateinit var gameRepository: GameRepository

    @Autowired
    private lateinit var playerRepository: PlayerRepository

    @Autowired
    private lateinit var roundStatusEventHandler: RoundStatusEventHandler

    private val preconfiguredPlayersFactory = PreconfiguredPlayersFactory()

    private val gameId = UUID.randomUUID()
    private val gameWorldId = null
    private val roundId = UUID.randomUUID()
    private val roundNumber = 10

    @Test
    fun test_awardTrophiesAtGameEnd() {
        val game = Game(gameId, gameWorldId, GameStatus.STARTED)
        val player = preconfiguredPlayersFactory.createAchievingPlayerPair(AchievementType.TRAVELING_BRONZE).second
        playerRepository.savePlayer(player)
        player.joinGame(game)
        gameRepository.saveGame(game)
        roundStatusEventHandler.handleRoundStatusEvent(RoundStatusEvent(gameId, roundId, roundNumber, RoundStatus.STARTED, Instant.now()))
        playerRepository.getPlayerById(player.id)!!.let {
            assertThat(it.achievements).isEmpty()
        }

        roundStatusEventHandler.handleRoundStatusEvent(RoundStatusEvent(gameId, roundId, roundNumber, RoundStatus.ENDED, Instant.now()))

        playerRepository.getPlayerById(player.id)!!.let {
            assertThat(it.achievements).isNotEmpty
        }
    }

}