package io.archilab.themicroservicedungeon.gamelog.domain.broker

import TestWithH2AndWithoutKafka
import io.archilab.themicroservicedungeon.gamelog.domain.entities.Game
import io.archilab.themicroservicedungeon.gamelog.domain.entities.Player
import io.archilab.themicroservicedungeon.gamelog.domain.entities.Round
import io.archilab.themicroservicedungeon.gamelog.domain.enums.GameStatus
import io.archilab.themicroservicedungeon.gamelog.domain.enums.RoundStatus
import io.archilab.themicroservicedungeon.gamelog.domain.events.game.GameStatusEvent
import io.archilab.themicroservicedungeon.gamelog.domain.events.game.PlayerStatusEvent
import io.archilab.themicroservicedungeon.gamelog.domain.events.game.RoundStatusEvent
import io.archilab.themicroservicedungeon.gamelog.domain.repositories.GameRepository
import io.archilab.themicroservicedungeon.gamelog.domain.repositories.PlayerRepository
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.transaction.annotation.Transactional
import java.time.Duration
import java.time.Instant
import java.util.*

@Transactional
class InternalEventListenerTestForGameDomain : TestWithH2AndWithoutKafka() {

    @Autowired
    private lateinit var internalEventListener: InternalEventListener

    @Autowired
    private lateinit var gameRepository: GameRepository

    @Autowired
    private lateinit var playerRepository: PlayerRepository

    private val gameId = UUID.randomUUID()
    private val gameWorldId = UUID.randomUUID()
    private val playerId = UUID.randomUUID()
    private val playerName = "ahahn94"
    private val timestamp = Instant.now()

    @Test
    fun test_gameStatusEvent_CREATED() {
        val status = GameStatus.CREATED
        val gameStatusEvent = GameStatusEvent(gameId, gameWorldId, status, timestamp)
        internalEventListener.listenForInternalEvents(gameStatusEvent)
        val gameFromDatabase = gameRepository.getGameById(gameId)
        assertThat(gameFromDatabase).usingRecursiveComparison().isEqualTo(Game(gameId, gameWorldId, status))
    }

    @Test
    fun test_gameStatusEvent_STARTED() {
        val status = GameStatus.STARTED
        val gameStatusEvent = GameStatusEvent(gameId, gameWorldId, status, timestamp)
        internalEventListener.listenForInternalEvents(gameStatusEvent)
        val gameFromDatabase = gameRepository.getGameById(gameId)
        val expected = Game(gameId, gameWorldId, status).apply { startGame(timestamp) }
        assertThat(gameFromDatabase).usingRecursiveComparison().isEqualTo(expected)
    }

    @Test
    fun test_gameStatusEvent_ENDED() {
        val status = GameStatus.ENDED
        val gameStatusEvent = GameStatusEvent(gameId, gameWorldId, status, timestamp)
        internalEventListener.listenForInternalEvents(gameStatusEvent)
        val gameFromDatabase = gameRepository.getGameById(gameId)
        val expected = Game(gameId, gameWorldId, status).apply { endGame(timestamp) }
        assertThat(gameFromDatabase).usingRecursiveComparison().isEqualTo(expected)
    }

    @Test
    fun test_playerStatusEvent_playerCreated() {
        val game = createGame()!!
        val playerStatusEvent = PlayerStatusEvent(game.id, playerId, playerName)

        internalEventListener.listenForInternalEvents(playerStatusEvent)

        val playerFromDatabase = playerRepository.getPlayerById(playerId)!!
        val gameFromDatabase = gameRepository.getGameById(gameId)
        val expected = Player(playerId, playerName).apply { joinGame(game) }
        assertThat(playerFromDatabase).usingRecursiveComparison().isEqualTo(expected)
        assertThat(gameFromDatabase?.players).contains(playerFromDatabase)
    }

    private fun createGame(): Game? {
        val status = GameStatus.CREATED
        val gameStatusEvent = GameStatusEvent(gameId, gameWorldId, status, timestamp)
        internalEventListener.listenForInternalEvents(gameStatusEvent)
        return gameRepository.getGameById(gameId)
    }

    @Test
    fun test_roundStatusEvent_roundStarted() {
        val roundId = UUID.randomUUID()
        val roundNumber = 42
        val roundStatus = RoundStatus.STARTED
        val timestamp = Instant.now()
        val roundStatusEvent = RoundStatusEvent(gameId, roundId, roundNumber, roundStatus, timestamp)

        internalEventListener.listenForInternalEvents(GameStatusEvent(gameId, gameWorldId, GameStatus.STARTED, timestamp))
        internalEventListener.listenForInternalEvents(roundStatusEvent)

        val expected = Round(gameId, roundId, roundNumber, roundStatus, timestamp)
        val result = gameRepository.getGameById(gameId)
        assertThat(result).isNotNull
        result?.run {
            assertThat(result.rounds.firstOrNull()).usingRecursiveComparison().isEqualTo(expected)
        }
    }

    @Test
    fun test_roundStatusEvent_commandInputEnded() {
        val roundId = UUID.randomUUID()
        val roundNumber = 42
        val roundStatus = RoundStatus.COMMAND_INPUT_ENDED
        val timestamp = Instant.now()
        val roundStatusEvent = RoundStatusEvent(gameId, roundId, roundNumber, roundStatus, timestamp)

        internalEventListener.listenForInternalEvents(GameStatusEvent(gameId, gameWorldId, GameStatus.STARTED, timestamp))
        internalEventListener.listenForInternalEvents(
            RoundStatusEvent(
                gameId,
                roundId,
                roundNumber,
                RoundStatus.STARTED,
                timestamp
            )
        )
        internalEventListener.listenForInternalEvents(roundStatusEvent)

        val expected = Round(gameId, roundId, roundNumber, roundStatus, timestamp).apply {
            commandInputEnded(timestamp)
        }
        val result = gameRepository.getGameById(gameId)
        assertThat(result).isNotNull
        result?.run {
            assertThat(result.rounds.firstOrNull()).usingRecursiveComparison().isEqualTo(expected)
        }
    }

    @Test
    fun test_roundStatusEvent_roundEnded() {
        val roundId = UUID.randomUUID()
        val roundNumber = 42
        val roundStatus = RoundStatus.ENDED
        val timestamp = Instant.now()
        val roundStatusEvent = RoundStatusEvent(gameId, roundId, roundNumber, roundStatus, timestamp)

        internalEventListener.listenForInternalEvents(GameStatusEvent(gameId, gameWorldId, GameStatus.STARTED, timestamp))
        internalEventListener.listenForInternalEvents(
            RoundStatusEvent(
                gameId,
                roundId,
                roundNumber,
                RoundStatus.STARTED,
                timestamp
            )
        )
        internalEventListener.listenForInternalEvents(roundStatusEvent)

        val expected = Round(gameId, roundId, roundNumber, roundStatus, timestamp).apply {
            roundEnded(timestamp)
        }
        val result = gameRepository.getGameById(gameId)
        assertThat(result).isNotNull
        result?.run {
            assertThat(result.rounds.firstOrNull()).usingRecursiveComparison().isEqualTo(expected)
        }
    }

    @Test
    fun test_getCurrentGame() {
        val game1Id = UUID.randomUUID()
        val game1Started = Instant.now().minus(Duration.ofMinutes(60))
        val game1Running = Instant.now().minus(Duration.ofMinutes(55))
        val game1Ended = Instant.now().minus(Duration.ofMinutes(50))
        val game2Id = UUID.randomUUID()
        val game2Started = Instant.now().minus(Duration.ofMinutes(45))
        val game2Running = Instant.now().minus(Duration.ofMinutes(30))
        val game2Ended = Instant.now().minus(Duration.ofMinutes(35))

        val noGameYet = gameRepository.getGameRunningAtTimestamp(game1Started)
        assertThat(noGameYet).isNull()

        checkGameBoundaries(game1Id, game1Started, game1Running, game1Ended)
        checkGameBoundaries(game2Id, game2Started, game2Running, game2Ended)
    }

    @Test
    fun test_getRunningRound() {
        val gameStartedTimestamp = Instant.now().minus(Duration.ofMinutes(10))
        createAndStartGame(gameId, gameWorldId, gameStartedTimestamp)

        val round1Id = UUID.randomUUID()
        val round1Started = Instant.now().minus(Duration.ofMinutes(9))
        val round1Ended = Instant.now().minus(Duration.ofMinutes(8))

        val round2Id = UUID.randomUUID()
        val round2Started = Instant.now().minus(Duration.ofMinutes(7))
        val round2Ended = Instant.now().minus(Duration.ofMinutes(6))

        val game = gameRepository.getGameById(gameId)
        assertThat(game).isNotNull
        game?.run {
            assertThat(runningRound).isNull()
        }

        checkRoundBoundaries(gameId, round1Id, 1, round1Started, round1Ended)
        checkRoundBoundaries(gameId, round2Id, 2, round2Started, round2Ended)
    }

    private fun checkRoundBoundaries(gameId: UUID, roundId: UUID, roundNumber: Int, start: Instant, end: Instant) {
        val commandInputEnd = start.plus(Duration.ofSeconds((end.epochSecond - start.epochSecond) / 2))
        val roundStarted = RoundStatusEvent(gameId, roundId, roundNumber, RoundStatus.STARTED, start)
        val commandInputEnded = RoundStatusEvent(gameId, roundId, roundNumber, RoundStatus.COMMAND_INPUT_ENDED, commandInputEnd)
        val roundEnded = RoundStatusEvent(gameId, roundId, roundNumber, RoundStatus.ENDED, end)

        internalEventListener.listenForInternalEvents(roundStarted)

        var game = gameRepository.getGameById(gameId)
        assertThat(game).isNotNull
        game?.run {
            assertThat(runningRound).isEqualTo(rounds.firstOrNull { round -> round.roundId == roundId })
        }

        internalEventListener.listenForInternalEvents(commandInputEnded)
        game = gameRepository.getGameById(gameId)
        assertThat(game).isNotNull
        game?.run {
            assertThat(runningRound).isEqualTo(rounds.firstOrNull { round -> round.roundId == roundId })
        }

        internalEventListener.listenForInternalEvents(roundEnded)
        game = gameRepository.getGameById(gameId)
        assertThat(game).isNotNull
        game?.run {
            assertThat(runningRound).isNull()
        }

    }

    private fun checkGameBoundaries(gameId: UUID, startTimestamp: Instant, duringTimestamp: Instant, endTimestamp: Instant) {
        createAndStartGame(gameId, gameWorldId, startTimestamp)
        val oneSecondBeforeGame = gameRepository.getGameRunningAtTimestamp(startTimestamp.minusSeconds(1))
        oneSecondBeforeGame?.run {
            assertThat(id).isNotEqualTo(gameId)
        }
        val startOfGame1 = gameRepository.getGameRunningAtTimestamp(startTimestamp)
        assertGameProperties(startOfGame1, gameId, GameStatus.STARTED, startTimestamp, null)
        val duringGame = gameRepository.getGameRunningAtTimestamp(duringTimestamp)
        assertGameProperties(duringGame, gameId, GameStatus.STARTED, startTimestamp, null)
        endGame(gameId, gameWorldId, endTimestamp)
        val endOfGame = gameRepository.getGameRunningAtTimestamp(endTimestamp)
        assertGameProperties(endOfGame, gameId, GameStatus.ENDED, startTimestamp, endTimestamp)
        val oneSecondAfterGame = gameRepository.getGameRunningAtTimestamp(endTimestamp.plusSeconds(1))
        oneSecondAfterGame?.run {
            assertThat(id).isNotEqualTo(gameId)
        }
    }

    private fun assertGameProperties(
        game: Game?,
        gameId: UUID,
        gameStatus: GameStatus,
        startTimestamp: Instant,
        endTimestamp: Instant?
    ) {
        assertThat(game).isNotNull
        game?.let {
            assertThat(it.id).isEqualTo(gameId)
            assertThat(it.gameStatus).isEqualTo(gameStatus)
            assertThat(it.startedAt).isEqualTo(startTimestamp)
            assertThat(it.endedAt).isEqualTo(endTimestamp)
        }
    }

    private fun createAndStartGame(gameId: UUID, gameWorldId: UUID, timestamp: Instant) {
        val gameCreatedEvent = GameStatusEvent(gameId, gameWorldId, GameStatus.CREATED, timestamp)
        internalEventListener.listenForInternalEvents(gameCreatedEvent)
        val gameStartedEvent = GameStatusEvent(gameId, gameWorldId, GameStatus.STARTED, timestamp)
        internalEventListener.listenForInternalEvents(gameStartedEvent)
    }

    private fun endGame(gameId: UUID, gameWorldId: UUID, timestamp: Instant) {
        val gameEndedEvent = GameStatusEvent(gameId, gameWorldId, GameStatus.ENDED, timestamp)
        internalEventListener.listenForInternalEvents(gameEndedEvent)
    }

}