package io.archilab.themicroservicedungeon.gamelog.domain.broker

import TestWithH2AndWithoutKafka
import io.archilab.themicroservicedungeon.gamelog.domain.entities.Planet
import io.archilab.themicroservicedungeon.gamelog.domain.entities.Resource
import io.archilab.themicroservicedungeon.gamelog.domain.entities.World
import io.archilab.themicroservicedungeon.gamelog.domain.enums.ResourceType
import io.archilab.themicroservicedungeon.gamelog.domain.enums.WorldStatus
import io.archilab.themicroservicedungeon.gamelog.domain.events.map.GameWorldCreatedEvent
import io.archilab.themicroservicedungeon.gamelog.domain.events.map.GameWorldDeletedEvent
import io.archilab.themicroservicedungeon.gamelog.domain.events.map.GameWorldStatusChangedEvent
import io.archilab.themicroservicedungeon.gamelog.domain.events.map.ResourceMinedEvent
import io.archilab.themicroservicedungeon.gamelog.domain.events.map.embedded.GameWorldPlanet
import io.archilab.themicroservicedungeon.gamelog.domain.events.map.embedded.GameWorldResource
import io.archilab.themicroservicedungeon.gamelog.domain.mappers.GameWorldPlanetToPlanetMapper
import io.archilab.themicroservicedungeon.gamelog.domain.repositories.WorldRepository
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.transaction.annotation.Transactional
import java.util.*

@Transactional
class InternalEventListenerTestForMapDomain : TestWithH2AndWithoutKafka() {

    private val worldId = UUID.randomUUID()

    @Autowired
    private lateinit var internalEventListener: InternalEventListener

    @Autowired
    private lateinit var worldRepository: WorldRepository

    @Autowired
    private lateinit var gameWorldPlanetToPlanetMapper: GameWorldPlanetToPlanetMapper

    private final val gameWorldPlanetCoal =
        GameWorldPlanet(UUID.randomUUID(), 0, 0, 1, GameWorldResource(ResourceType.COAL, 100, 100))
    private final val gameWorldPlanetIron =
        GameWorldPlanet(UUID.randomUUID(), 0, 1, 1, GameWorldResource(ResourceType.IRON, 100, 100))
    private final val gameWorldPlanetGem =
        GameWorldPlanet(UUID.randomUUID(), 1, 0, 1, GameWorldResource(ResourceType.GEM, 100, 100))
    private final val gameWorldPlanetGold =
        GameWorldPlanet(UUID.randomUUID(), 1, 1, 1, GameWorldResource(ResourceType.GOLD, 100, 100))

    private final val worldStatus = WorldStatus.ACTIVE
    private final val gameWorldPlanets = listOf(
        gameWorldPlanetCoal,
        gameWorldPlanetIron,
        gameWorldPlanetGem,
        gameWorldPlanetGold
    )
    val gameWorldCreatedEvent = GameWorldCreatedEvent(worldId, worldStatus, gameWorldPlanets)

    @Test
    fun test_gameWorldCreatedEvent() {
        internalEventListener.listenForInternalEvents(gameWorldCreatedEvent)

        val result = worldRepository.getWorldById(worldId)
        val planets = gameWorldPlanetToPlanetMapper.mapList(gameWorldPlanets)
        val expected = World(worldId, worldStatus, planets.toTypedArray().toSet())
        assertThat(result).usingRecursiveComparison().ignoringCollectionOrder().isEqualTo(expected)
    }

    @Test
    fun test_gameWorldDeletedEvent() {
        val gameWorldDeletedEvent = GameWorldDeletedEvent(worldId)
        internalEventListener.listenForInternalEvents(gameWorldCreatedEvent)
        internalEventListener.listenForInternalEvents(gameWorldDeletedEvent)

        val result = worldRepository.getWorldById(worldId)
        assertThat(result).isNotNull
    }

    @Test
    fun test_gameWorldStatusChangedEvent() {
        val newStatus = WorldStatus.INACTIVE
        val gameWorldStatusChangedEvent = GameWorldStatusChangedEvent(worldId, newStatus)

        internalEventListener.listenForInternalEvents(gameWorldCreatedEvent)
        internalEventListener.listenForInternalEvents(gameWorldStatusChangedEvent)

        val world = worldRepository.getWorldById(worldId)
        assertThat(world).isNotNull
        assertThat(world?.status).isEqualTo(newStatus)
    }

    @Test
    fun test_resourceMinedEvent() {
        internalEventListener.listenForInternalEvents(gameWorldCreatedEvent)
        val minedAmount = 10
        val changedResource = gameWorldPlanetCoal.resource?.let {
            GameWorldResource(it.resourceType, it.maxAmount, it.currentAmount - minedAmount)
        }
        val resourceMinedEvent = ResourceMinedEvent(gameWorldPlanetCoal.planetId, minedAmount, changedResource!!)
        val expectedPlanet = gameWorldPlanetCoal.let { planet ->
            Planet(
                planet.planetId,
                planet.x,
                planet.y,
                planet.movementDifficulty,
                with(changedResource) { hashMapOf(Pair(resourceType, Resource(resourceType, maxAmount, currentAmount))) })
        }

        internalEventListener.listenForInternalEvents(resourceMinedEvent)

        val world = worldRepository.getWorldById(worldId)
        assertThat(world).isNotNull
        assertThat(world?.planets).contains(expectedPlanet)
    }

}