package io.archilab.themicroservicedungeon.gamelog.domain.broker

import TestWithH2AndWithoutKafka
import io.archilab.themicroservicedungeon.gamelog.domain.entities.Price
import io.archilab.themicroservicedungeon.gamelog.domain.entities.TradingTransaction
import io.archilab.themicroservicedungeon.gamelog.domain.enums.BankAccountTransactionType
import io.archilab.themicroservicedungeon.gamelog.domain.enums.GameStatus
import io.archilab.themicroservicedungeon.gamelog.domain.enums.TradableType
import io.archilab.themicroservicedungeon.gamelog.domain.enums.TradingTransactionType
import io.archilab.themicroservicedungeon.gamelog.domain.events.game.GameStatusEvent
import io.archilab.themicroservicedungeon.gamelog.domain.events.game.PlayerStatusEvent
import io.archilab.themicroservicedungeon.gamelog.domain.events.trading.*
import io.archilab.themicroservicedungeon.gamelog.domain.events.trading.embedded.TradablePrice
import io.archilab.themicroservicedungeon.gamelog.domain.repositories.PlayerRepository
import io.archilab.themicroservicedungeon.gamelog.domain.repositories.PriceRepository
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.transaction.annotation.Transactional
import java.math.BigDecimal
import java.time.Instant
import java.util.*

@Transactional
class InternalEventListenerTestForTradingDomain : TestWithH2AndWithoutKafka() {

    @Autowired
    private lateinit var internalEventListener: InternalEventListener

    @Autowired
    private lateinit var playerRepository: PlayerRepository

    @Autowired
    private lateinit var priceRepository: PriceRepository

    private val gameId = UUID.randomUUID()
    private val gameWorldId = UUID.randomUUID()
    private val playerId = UUID.randomUUID()
    private val playerName = "foobar"
    private val robotId = UUID.randomUUID()
    private val startBalance = 0
    private val tradableName = "itemName"

    @Test
    fun test_bankAccountInitializedEvent() {
        prepareGameAndPlayer()
        setBankAccountBalanceForPlayer(this.playerId, 100)
        val event = BankAccountInitializedEvent(playerId, startBalance, Instant.now())

        internalEventListener.listenForInternalEvents(event)

        val playerAfterEvent = playerRepository.getPlayerById(playerId)
        assertThat(playerAfterEvent).isNotNull
        playerAfterEvent?.run {
            assertThat(bankAccountBalance).isEqualTo(0)
        }
    }

    @Test
    fun test_bankAccountClearedEvent() {
        prepareGameAndPlayer()
        setBankAccountBalanceForPlayer(this.playerId, 100)
        val event = BankAccountClearedEvent(playerId, startBalance, Instant.now())

        internalEventListener.listenForInternalEvents(event)

        val playerAfterEvent = playerRepository.getPlayerById(playerId)
        assertThat(playerAfterEvent).isNotNull
        playerAfterEvent?.run {
            assertThat(bankAccountBalance).isEqualTo(0)
        }
    }

    @Test
    fun test_bankAccountTransactionBookedEvent() {
        prepareGameAndPlayer()
        val balance = 100
        setBankAccountBalanceForPlayer(this.playerId, balance.toLong())
        val amount = 25
        val event = BankAccountTransactionBookedEvent(playerId, amount, balance + amount, Instant.now())

        internalEventListener.listenForInternalEvents(event)

        val playerAfterEvent = playerRepository.getPlayerById(playerId)
        assertThat(playerAfterEvent).isNotNull
        playerAfterEvent?.run {
            assertThat(bankAccountBalance).isEqualTo((balance + amount).toLong())
        }
    }

    @Test
    fun test_tradablePricesEvent() {
        prepareGameAndPlayer()
        val event = createTradablePricesList()

        internalEventListener.listenForInternalEvents(event)

        val pricesList = priceRepository.getNewestPrices()!!
        assertThat(pricesList.prices).isNotEmpty
        val expectedPrices = event.map {
            Price(it.name, it.price, it.type)
        }
        assertThat(pricesList.prices).usingRecursiveComparison().ignoringCollectionOrder().isEqualTo(expectedPrices)
    }

    @Test
    fun test_tradableBoughtEvent() {
        prepareGameAndPlayer()
        val amount = 1
        val pricePerUnit = BigDecimal(10)
        val type = TradableType.ITEM
        val totalPrice = pricePerUnit * amount.toBigDecimal()
        val event = TradableBoughtEvent(playerId, robotId, type, tradableName, amount, pricePerUnit, totalPrice)

        internalEventListener.listenForInternalEvents(event)

        val player = playerRepository.getPlayerById(playerId)
        assertThat(player).isNotNull
        player?.run {
            val expected = TradingTransaction(
                robotId,
                gameId,
                type,
                TradingTransactionType.BOUGHT,
                tradableName,
                amount,
                pricePerUnit,
                totalPrice
            )
            assertThat(tradingTransactions.lastOrNull()).usingRecursiveComparison().isEqualTo(expected)
        }
    }

    @Test
    fun test_tradableSoldEvent() {
        prepareGameAndPlayer()
        val amount = 1
        val pricePerUnit = BigDecimal(10)
        val type = TradableType.ITEM
        val totalPrice = pricePerUnit * amount.toBigDecimal()
        val event = TradableSoldEvent(playerId, robotId, type, tradableName, amount, pricePerUnit, totalPrice)

        internalEventListener.listenForInternalEvents(event)

        val player = playerRepository.getPlayerById(playerId)
        assertThat(player).isNotNull
        player?.run {
            val expected = TradingTransaction(
                robotId,
                gameId,
                type,
                TradingTransactionType.SOLD,
                tradableName,
                amount,
                pricePerUnit,
                totalPrice
            )
            assertThat(tradingTransactions.lastOrNull()).usingRecursiveComparison().isEqualTo(expected)
        }
    }

    private fun setBankAccountBalanceForPlayer(playerId: UUID, newBalance: Long) {
        val playerBeforeEvent = playerRepository.getPlayerById(playerId)
        playerBeforeEvent?.apply {
            bookBankAccountTransaction(BankAccountTransactionType.INITIALIZED, newBalance, newBalance, Instant.now())
            playerRepository.savePlayer(this)
        }
    }

    private fun prepareGameAndPlayer() {
        internalEventListener.listenForInternalEvents(GameStatusEvent(gameId, gameWorldId, GameStatus.STARTED, Instant.now()))
        internalEventListener.listenForInternalEvents(PlayerStatusEvent(gameId, playerId, playerName))
    }

    private fun createTradablePricesList(): TradablePricesEvent {
        return TradablePricesEvent().apply {
            add(TradablePrice("MINING_SPEED_1", BigDecimal(50), TradableType.UPGRADE))
            add(TradablePrice("GOLD", BigDecimal(50), TradableType.RESOURCE))
            add(TradablePrice("MINING_SPEED_2", BigDecimal(300), TradableType.UPGRADE))
            add(TradablePrice("HEALTH_RESTORE", BigDecimal(50), TradableType.RESTORATION))
            add(TradablePrice("MINING_SPEED_5", BigDecimal(15000), TradableType.UPGRADE))
            add(TradablePrice("PLATIN", BigDecimal(60), TradableType.RESOURCE))
            add(TradablePrice("MINING_SPEED_3", BigDecimal(1500), TradableType.UPGRADE))
            add(TradablePrice("MAX_ENERGY_4", BigDecimal(4000), TradableType.UPGRADE))
            add(TradablePrice("MINING_SPEED_4", BigDecimal(4000), TradableType.UPGRADE))
            add(TradablePrice("MAX_ENERGY_5", BigDecimal(15000), TradableType.UPGRADE))
            add(TradablePrice("COAL", BigDecimal(5), TradableType.RESOURCE))
            add(TradablePrice("MAX_ENERGY_2", BigDecimal(300), TradableType.UPGRADE))
            add(TradablePrice("MAX_ENERGY_3", BigDecimal(1500), TradableType.UPGRADE))
            add(TradablePrice("ENERGY_REGEN_5", BigDecimal(15000), TradableType.UPGRADE))
            add(TradablePrice("MAX_ENERGY_1", BigDecimal(50), TradableType.UPGRADE))
            add(TradablePrice("STORAGE_1", BigDecimal(50), TradableType.UPGRADE))
            add(TradablePrice("ENERGY_REGEN_2", BigDecimal(300), TradableType.UPGRADE))
            add(TradablePrice("ENERGY_REGEN_1", BigDecimal(50), TradableType.UPGRADE))
            add(TradablePrice("STORAGE_3", BigDecimal(1500), TradableType.UPGRADE))
            add(TradablePrice("ENERGY_REGEN_4", BigDecimal(4000), TradableType.UPGRADE))
            add(TradablePrice("STORAGE_2", BigDecimal(300), TradableType.UPGRADE))
            add(TradablePrice("ENERGY_REGEN_3", BigDecimal(1500), TradableType.UPGRADE))
            add(TradablePrice("STORAGE_5", BigDecimal(15000), TradableType.UPGRADE))
            add(TradablePrice("STORAGE_4", BigDecimal(4000), TradableType.UPGRADE))
            add(TradablePrice("MINING_3", BigDecimal(1500), TradableType.UPGRADE))
            add(TradablePrice("MINING_4", BigDecimal(4000), TradableType.UPGRADE))
            add(TradablePrice("ROBOT", BigDecimal(100), TradableType.ITEM))
            add(TradablePrice("MINING_5", BigDecimal(15000), TradableType.UPGRADE))
            add(TradablePrice("MINING_1", BigDecimal(50), TradableType.UPGRADE))
            add(TradablePrice("MINING_2", BigDecimal(300), TradableType.UPGRADE))
            add(TradablePrice("IRON", BigDecimal(15), TradableType.RESOURCE))
            add(TradablePrice("HEALTH_2", BigDecimal(300), TradableType.UPGRADE))
            add(TradablePrice("HEALTH_1", BigDecimal(50), TradableType.UPGRADE))
            add(TradablePrice("GEM", BigDecimal(30), TradableType.RESOURCE))
            add(TradablePrice("HEALTH_5", BigDecimal(15000), TradableType.UPGRADE))
            add(TradablePrice("ENERGY_RESTORE", BigDecimal(75), TradableType.RESTORATION))
            add(TradablePrice("HEALTH_4", BigDecimal(4000), TradableType.UPGRADE))
            add(TradablePrice("HEALTH_3", BigDecimal(1500), TradableType.UPGRADE))
            add(TradablePrice("DAMAGE_5", BigDecimal(15000), TradableType.UPGRADE))
            add(TradablePrice("DAMAGE_4", BigDecimal(4000), TradableType.UPGRADE))
            add(TradablePrice("DAMAGE_3", BigDecimal(1500), TradableType.UPGRADE))
            add(TradablePrice("DAMAGE_2", BigDecimal(300), TradableType.UPGRADE))
            add(TradablePrice("DAMAGE_1", BigDecimal(50), TradableType.UPGRADE))
        }
    }

}