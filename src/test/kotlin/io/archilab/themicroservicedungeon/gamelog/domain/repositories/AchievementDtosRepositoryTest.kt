package io.archilab.themicroservicedungeon.gamelog.domain.repositories

import TestWithH2AndWithoutKafka
import io.archilab.themicroservicedungeon.gamelog.domain.achievements.AchievementsManager
import io.archilab.themicroservicedungeon.gamelog.domain.dtos.AchievementDto
import io.archilab.themicroservicedungeon.gamelog.domain.dtos.PlayerAchievementDto
import io.archilab.themicroservicedungeon.gamelog.domain.dtos.PlayerDto
import io.archilab.themicroservicedungeon.gamelog.domain.entities.Game
import io.archilab.themicroservicedungeon.gamelog.domain.enums.AchievementType
import io.archilab.themicroservicedungeon.gamelog.domain.enums.GameStatus
import io.archilab.themicroservicedungeon.gamelog.domain.enums.RoundStatus
import io.archilab.themicroservicedungeon.gamelog.domain.eventhandlers.RoundStatusEventHandler
import io.archilab.themicroservicedungeon.gamelog.domain.events.game.RoundStatusEvent
import io.archilab.themicroservicedungeon.gamelog.domain.factories.PreconfiguredPlayersFactory
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.transaction.annotation.Transactional
import java.time.Instant
import java.util.*

@Transactional
class AchievementDtosRepositoryTest : TestWithH2AndWithoutKafka() {

    @Autowired
    private lateinit var achievementsRepository: AchievementDtosRepository

    @Autowired
    private lateinit var gameRepository: GameRepository

    @Autowired
    private lateinit var playerRepository: PlayerRepository

    @Autowired
    private lateinit var achievementsManager: AchievementsManager

    @Autowired
    private lateinit var roundStatusEventHandler: RoundStatusEventHandler

    private val playersFactory = PreconfiguredPlayersFactory()

    private val gameId = UUID.randomUUID()
    private val gameWorldId = null
    private val roundId = UUID.randomUUID()
    private val roundNumber = 1

    @Test
    @Transactional
    fun test_getTrophiesForLastGame() {
        val game = Game(gameId, gameWorldId, GameStatus.CREATED).apply { startGame(Instant.now()) }
        gameRepository.saveGame(game)
        val player = playersFactory.createAchievingPlayerPair(AchievementType.TRAVELING_BRONZE).second
        playerRepository.savePlayer(player)
        player.joinGame(game)
        game.newRound(roundId, roundNumber, Instant.now().minusSeconds(60))
        game.updateRound(roundId, RoundStatus.ENDED, Instant.now())
        playerRepository.savePlayer(player)
        gameRepository.saveGame(game)
        roundStatusEventHandler.handleRoundStatusEvent(RoundStatusEvent(gameId, roundId, roundNumber, RoundStatus.ENDED, Instant.now()))

        val achievementsDto = achievementsRepository.getAchievements()

        assertThat(achievementsDto.gameId).isEqualTo(gameId)
        assertThat(achievementsDto.playerAchievements).isNotEmpty
        assertThat(achievementsDto.playerAchievements.firstOrNull()).isEqualTo(
            PlayerAchievementDto(
                player.let { PlayerDto(it.id, it.name) },
                gameId,
                achievementsManager.getAchievement(AchievementType.TRAVELING_BRONZE)!!.let {
                    AchievementDto(it.name, it.imageUrl, achievementsManager.getAchievementCategory(AchievementType.TRAVELING_BRONZE))
                })
        )
    }

}