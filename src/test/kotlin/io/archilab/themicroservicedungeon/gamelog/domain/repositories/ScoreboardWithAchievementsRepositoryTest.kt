package io.archilab.themicroservicedungeon.gamelog.domain.repositories

import TestWithH2AndWithoutKafka
import io.archilab.themicroservicedungeon.gamelog.domain.achievements.AchievementsStrategy
import io.archilab.themicroservicedungeon.gamelog.domain.entities.Game
import io.archilab.themicroservicedungeon.gamelog.domain.entities.Player
import io.archilab.themicroservicedungeon.gamelog.domain.enums.GameStatus
import io.archilab.themicroservicedungeon.gamelog.domain.enums.PlayerSuccess
import io.archilab.themicroservicedungeon.gamelog.domain.enums.RoundStatus
import io.archilab.themicroservicedungeon.gamelog.domain.enums.ScoreboardCategory
import io.archilab.themicroservicedungeon.gamelog.domain.factories.PreconfiguredPlayersFactory
import io.archilab.themicroservicedungeon.gamelog.domain.scoreboard.ScoreboardStrategy
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.transaction.annotation.Transactional
import java.time.Instant
import java.util.*

@Transactional
class ScoreboardWithAchievementsRepositoryTest : TestWithH2AndWithoutKafka() {

    @Autowired
    private lateinit var scoreboardWithAchievementsRepository: ScoreboardWithAchievementsRepository

    @Autowired
    private lateinit var gameRepository: GameRepository

    @Autowired
    private lateinit var playerRepository: PlayerRepository

    @Autowired
    private lateinit var scoreboardStrategy: ScoreboardStrategy

    @Autowired
    private lateinit var achievementsStrategy: AchievementsStrategy

    private val preconfiguredPlayersFactory = PreconfiguredPlayersFactory()

    private val gameId = UUID.randomUUID()
    private val roundId = UUID.randomUUID()
    private val roundNumber = 42

    @Test
    fun test_getScoreboardWithAchievements() {
        val gameStatus = GameStatus.STARTED
        val game = createGame(gameId, null, gameStatus)
        val goodPlayer = preconfiguredPlayersFactory.createFocusedPlayer(ScoreboardCategory.GAME, PlayerSuccess.HIGH).also {
            registerPlayerForGame(it, game)
        }
        val mediumPlayer = preconfiguredPlayersFactory.createFocusedPlayer(ScoreboardCategory.GAME, PlayerSuccess.MEDIUM).also {
            registerPlayerForGame(it, game)
        }
        val badPlayer = preconfiguredPlayersFactory.createFocusedPlayer(ScoreboardCategory.GAME, PlayerSuccess.LOW).also {
            registerPlayerForGame(it, game)
        }
        startGame(game)

        val scoreboardWithAchievements = scoreboardWithAchievementsRepository.getScoreboardWithAchievements()

        assertThat(scoreboardWithAchievements.gameId).isEqualTo(gameId)
        assertThat(scoreboardWithAchievements.gameStatus).isEqualTo(gameStatus)
        assertThat(scoreboardWithAchievements.roundNumber).isEqualTo(roundNumber)
        assertThat(scoreboardWithAchievements.scoreboardEntriesWithAchievements.size).isEqualTo(3)
        assertThat(scoreboardWithAchievements.scoreboardEntriesWithAchievements.map { it.player.id }).isEqualTo(listOf(goodPlayer.id, mediumPlayer.id, badPlayer.id))
        assertThat(scoreboardWithAchievements.scoreboardEntriesWithAchievements.firstOrNull()?.achievements).isNotEmpty
    }

    private fun createGame(gameId: UUID, gameWorldId: UUID?, gameStatus: GameStatus) : Game {
        val game = Game(gameId, gameWorldId, gameStatus)
        gameRepository.saveGame(game)
        return game
    }

    private fun registerPlayerForGame(player: Player, game: Game) {
        playerRepository.savePlayer(player)
        player.joinGame(game)
        playerRepository.savePlayer(player)
        gameRepository.saveGame(game)
    }

    private fun startGame(game: Game) {
        game.startGame(Instant.now())
        game.newRound(roundId, roundNumber, Instant.now())
        game.updateRound(roundId, RoundStatus.ENDED, Instant.now())
        game.scoreboard = scoreboardStrategy.calculateScoreboard(game, roundNumber)
        game.players.forEach { player ->
            achievementsStrategy.awardAchievementsToPlayer(game.id, player)
            playerRepository.savePlayer(player)
        }

        gameRepository.saveGame(game)
    }

}