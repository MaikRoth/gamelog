package io.archilab.themicroservicedungeon.gamelog.domain.repositories

import TestWithH2AndWithoutKafka
import io.archilab.themicroservicedungeon.gamelog.domain.dtos.ScoreboardTrophyDto
import io.archilab.themicroservicedungeon.gamelog.domain.entities.Game
import io.archilab.themicroservicedungeon.gamelog.domain.entities.PlayerScore
import io.archilab.themicroservicedungeon.gamelog.domain.entities.Scoreboard
import io.archilab.themicroservicedungeon.gamelog.domain.enums.*
import io.archilab.themicroservicedungeon.gamelog.domain.eventhandlers.GameEventHandler
import io.archilab.themicroservicedungeon.gamelog.domain.events.game.GameStatusEvent
import io.archilab.themicroservicedungeon.gamelog.domain.factories.PreconfiguredPlayersFactory
import io.archilab.themicroservicedungeon.gamelog.domain.trophies.TrophiesManager
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.transaction.annotation.Transactional
import java.time.Instant
import java.util.*

@Transactional
class TrophiesRepositoryTest : TestWithH2AndWithoutKafka() {

    @Autowired
    private lateinit var scoreboardTrophiesRepository: ScoreboardTrophiesRepository

    @Autowired
    private lateinit var gameRepository: GameRepository

    @Autowired
    private lateinit var playerRepository: PlayerRepository

    @Autowired
    private lateinit var trophiesManager: TrophiesManager

    @Autowired
    private lateinit var gameEventHandler: GameEventHandler

    private val playersFactory = PreconfiguredPlayersFactory()

    private val gameId = UUID.randomUUID()
    private val gameWorldId = null
    private val roundId = UUID.randomUUID()

    @Test
    @Transactional
    fun test_getTrophiesForLastGame() {
        val game = Game(gameId, gameWorldId, GameStatus.CREATED)
        gameRepository.saveGame(game)
        val player1 = playersFactory.createFocusedPlayer(ScoreboardCategory.GAME, PlayerSuccess.HIGH)
        val player2 = playersFactory.createFocusedPlayer(ScoreboardCategory.GAME, PlayerSuccess.HIGH)
        playerRepository.savePlayer(player1)
        player1.joinGame(game)
        playerRepository.savePlayer(player2)
        player2.joinGame(game)
        game.newRound(roundId, 100, Instant.now().minusSeconds(60))
        game.updateRound(roundId, RoundStatus.ENDED, Instant.now())
        game.scoreboard = Scoreboard(
            gameId, 100, listOf(
                PlayerScore(player1, 100, 100, 100, 100, 100),
                PlayerScore(player2, 100, 100, 100, 100, 100)
            )
        )
        playerRepository.savePlayer(player1)
        playerRepository.savePlayer(player2)
        gameRepository.saveGame(game)
        gameEventHandler.handleGameEnded(GameStatusEvent(gameId, gameWorldId, GameStatus.ENDED, Instant.now()))

        val trophies = scoreboardTrophiesRepository.getScoreboardTrophies()

        assertThat(trophies.gameID).isEqualTo(gameId)
        assertThat(trophies.scoreboardTrophies).isNotEmpty
        assertThat(trophies.scoreboardTrophies.firstOrNull()?.scoreboardTrophies).usingRecursiveFieldByFieldElementComparator()
            .contains(
                trophiesManager.getTrophy(TrophyType.GameFirstPlace)!!
                    .let { ScoreboardTrophyDto(it.name, it.imageUrl, ScoreboardCategory.GAME) })
    }

}