package io.archilab.themicroservicedungeon.gamelog.domain.scoreboard

import TestWithH2AndWithoutKafka
import io.archilab.themicroservicedungeon.gamelog.domain.entities.*
import io.archilab.themicroservicedungeon.gamelog.domain.enums.GameStatus
import io.archilab.themicroservicedungeon.gamelog.domain.enums.PlayerSuccess
import io.archilab.themicroservicedungeon.gamelog.domain.enums.RoundStatus
import io.archilab.themicroservicedungeon.gamelog.domain.enums.ScoreboardCategory
import io.archilab.themicroservicedungeon.gamelog.domain.factories.PreconfiguredPlayersFactory
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import java.time.Instant
import java.util.*

class ScoreboardStrategyTest : TestWithH2AndWithoutKafka() {

    private val gameId = UUID.randomUUID()
    private val gameWorldId = null

    private val playersFactory = PreconfiguredPlayersFactory(1, 5, 10)

    @Autowired
    private lateinit var scoreboardStrategy: ScoreboardStrategy

    @Test
    fun test_miningScores() {
        testScoreboardCategory(ScoreboardCategory.MINING)
    }

    @Test
    fun test_fightingScores() {
        testScoreboardCategory(ScoreboardCategory.FIGHTING)
    }

    @Test
    fun test_tradingScore() {
        testScoreboardCategory(ScoreboardCategory.TRADING)
    }

    @Test
    fun test_travelingScore() {
        testScoreboardCategory(ScoreboardCategory.TRAVELING)
    }

    @Test
    fun test_gameScore() {
        testScoreboardCategory(ScoreboardCategory.GAME)
    }

    private fun testScoreboardCategory(scoreboardCategory: ScoreboardCategory) {
        val goodPlayer = playersFactory.createFocusedPlayer(scoreboardCategory, PlayerSuccess.HIGH)
        val mediumPlayer = playersFactory.createFocusedPlayer(scoreboardCategory, PlayerSuccess.MEDIUM)
        val badPlayer = playersFactory.createFocusedPlayer(scoreboardCategory, PlayerSuccess.LOW)
        val game = createGameAndRegisterPlayers(mediumPlayer, badPlayer, goodPlayer)
        val roundId = UUID.randomUUID()
        val roundNumber = 1
        game.newRound(roundId, roundNumber, Instant.now())
        game.updateRound(UUID.randomUUID(), RoundStatus.ENDED, Instant.now())

        val scoreboard = scoreboardStrategy.calculateScoreboard(game, roundNumber)
        val ranking = scoreboard.playerScores.sortedByDescending {
            when (scoreboardCategory) {
                ScoreboardCategory.FIGHTING -> it.fightingScore
                ScoreboardCategory.MINING -> it.miningScore
                ScoreboardCategory.GAME -> it.totalScore
                ScoreboardCategory.TRADING -> it.tradingScore
                ScoreboardCategory.TRAVELING -> it.travelingScore
            }
        }
        assertThat(ranking.size).isEqualTo(3)
        assertThat(ranking.get(0).player).isEqualTo(goodPlayer)
        assertThat(ranking.getOrNull(1)?.player).usingRecursiveComparison().isEqualTo(mediumPlayer)
        assertThat(ranking.getOrNull(2)?.player).usingRecursiveComparison().isEqualTo(badPlayer)
    }

    private fun createGameAndRegisterPlayers(
        vararg players: Player
    ): Game {
        val game = Game(gameId, gameWorldId, GameStatus.CREATED)
        for (player in players) {
            player.joinGame(game)
            // Manually set the gameID of the robots and transactions. Only necessary because player is configured before game starts.
            player.robots.forEach {
                it.gameId = gameId
            }
            player.tradingTransactions.forEach {
                it.gameId = gameId
            }
        }
        game.startGame(Instant.now())
        return game
    }

    private fun Scoreboard.string() : String {
        return "Scoreboard(gameId=$gameId, roundNumber=$roundNumber, playerScores=(${playerScores.map { it.string() }}))"
    }

    private fun PlayerScore.string(): String {
        return "PlayerScore(player=$player, totalScore=$totalScore, fightingScore=$fightingScore, miningScore=$miningScore, tradingScore=$tradingScore, travelingScore=$travelingScore)"
    }

}