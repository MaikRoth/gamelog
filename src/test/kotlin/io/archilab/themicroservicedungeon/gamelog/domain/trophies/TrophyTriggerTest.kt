package io.archilab.themicroservicedungeon.gamelog.domain.trophies

import TestWithH2AndWithoutKafka
import io.archilab.themicroservicedungeon.gamelog.domain.entities.Game
import io.archilab.themicroservicedungeon.gamelog.domain.entities.Player
import io.archilab.themicroservicedungeon.gamelog.domain.entities.PlayerScore
import io.archilab.themicroservicedungeon.gamelog.domain.entities.Scoreboard
import io.archilab.themicroservicedungeon.gamelog.domain.enums.GameStatus
import io.archilab.themicroservicedungeon.gamelog.domain.eventhandlers.GameEventHandler
import io.archilab.themicroservicedungeon.gamelog.domain.events.game.GameStatusEvent
import io.archilab.themicroservicedungeon.gamelog.domain.repositories.GameRepository
import io.archilab.themicroservicedungeon.gamelog.domain.repositories.PlayerRepository
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.transaction.annotation.Transactional
import java.time.Instant
import java.util.*

@Transactional
class TrophyTriggerTest: TestWithH2AndWithoutKafka() {

    @Autowired
    private lateinit var gameRepository: GameRepository

    @Autowired
    private lateinit var playerRepository: PlayerRepository

    @Autowired
    private lateinit var gameEventHandler: GameEventHandler

    private val gameId = UUID.randomUUID()
    private val gameWorldId = null
    private val playerId = UUID.randomUUID()

    @Test
    fun test_awardTrophiesAtGameEnd() {
        val game = Game(gameId, gameWorldId, GameStatus.STARTED)
        val player = Player(playerId, "player")
        playerRepository.savePlayer(player)
        player.joinGame(game)
        game.scoreboard = Scoreboard(gameId, 100, listOf(
            PlayerScore(player, 100, 100, 100, 100, 100)
        ))
        gameRepository.saveGame(game)
        assertThat(player.trophies).isEmpty()

        gameEventHandler.handleGameEnded(GameStatusEvent(gameId, gameWorldId, GameStatus.ENDED, Instant.now()))

        gameRepository.getGameById(gameId)?.run {
            assertThat(players.firstOrNull()?.trophies).isNotEmpty
        }
    }

}